// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using System.Drawing;
using System.Threading;
using System.Diagnostics;

namespace SymbolicChase
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the 
	// User Interface of the application, as well as listening (and optionally responding) to 
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		// class-level declarations
		UIWindow window;
		
		
		
		#region Fields
		
		private StockRepositoryEx _stockService = new StockRepositoryEx();
		private MainNavController navigationController;
		private UISplashView splashView;
		
		#endregion Fields
		
		
		
		
		#region Properties
		
		public static AppDelegate Self
		{
			get;
			private set;
		}//end static AppDelegate Self
		
		
		
		public bool AppStartFlag
		{
			get;
			private set;
		}//end bool AppStartFlag
		
		
		
		public DeviceType DeviceType
		{
			get
			{
				return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad ? DeviceType.iPad : DeviceType.iPhone;
			}//end get
			
		}//end DeviceType DeviceType
		

		public StockRepositoryEx StockService
		{
			get
			{
				return this._stockService;
			}//end get
			
		}//end IStockService StockService
		
		
		
		public UINavigationController NavController
		{
			
			get
			{
				return this.navigationController;
			}//end get
			
		}//end UINavigationController NavController
		
		
		
		// After first "get", set to true.
		public bool IsFirstRun
		{
			get
			{
				return !NSUserDefaults.StandardUserDefaults.BoolForKey("IsFirstRun");
			} set
			{
				NSUserDefaults.StandardUserDefaults.SetBool(value, "IsFirstRun");
			}//end get set
		}//end bool IsFirstRun




		// After first "get", set to true.
		public bool ToolTipShown
		{
			get
			{
				return NSUserDefaults.StandardUserDefaults.BoolForKey("ToolTipShown");
			} set
			{
				NSUserDefaults.StandardUserDefaults.SetBool(value, "ToolTipShown");
			}//end get set

		}//end bool ToolTipShown


		
		
		
		private DateTime referenceDate;
		public DateTime LastRefreshedDate
		{
			get
			{
				return referenceDate.AddSeconds(Convert.ToDouble(NSUserDefaults.StandardUserDefaults.IntForKey("LastRefreshedDate")));
			} set
			{
				NSUserDefaults.StandardUserDefaults.SetInt(Convert.ToInt32(value.Subtract(referenceDate).TotalSeconds), "LastRefreshedDate");
			}//end get set
			
		}//end DateTime LastRefreshedDate
		
		
		
		public bool RefreshRequired
		{
			get
			{
				
				TimeSpan ts = DateTime.Now.Subtract(AppDelegate.Self.LastRefreshedDate);
							
#if(DEBUG || ADHOC)
			
				if (ts.TotalMinutes >= 15)
				{
					return true;
				} else 
				{
					return false;
				}//end if else
			
#else
				if (ts.TotalHours >= 24)
				{
					return true;
				} else
				{
					return false;
				}//end if else
#endif
				
			}//end get
				
		}//end bool RefreshRequired
		
		
		
		public UIInterfaceOrientation LastInterfaceOrientation
		{
			get;
			set;
		}//end UIInterfaceOrientation LastInterfaceOrientation



		public RectangleF MainScreenBounds
		{
			get;
			private set;
		}//end RectangleF MainScreenBounds



		public bool IsOldIOS
		{
			get
			{
				Version ver = new Version(UIDevice.CurrentDevice.SystemVersion);
				if (ver.Major < 6)
				{
					return true;
				} else
				{
					return false;
				}//end if else

			}//end get

		}//end bool IsOldIOS



		public CurrencyHelper CurrencyHelper
		{
			get;
			private set;
		}//end CurrencyHelper CurrencyHelper
		
		#endregion Properties
		
		
		
		#region Public methods
		
		public void Initialise()
		{

			this._stockService.SetupDb();

			if (!FeedManager.IsFeedConnnected)
			{
				this.BeginInvokeOnMainThread(delegate {

					UIAlertView alert = new UIAlertView();
				    alert.Title = "You have no connection";
				    alert.AddButton("Ok");
				    alert.Message = "The application will now run off the in memory data";
				    alert.Show();

				});
			}

		}//end void Initialise
		
		
		
		
		public float DegreesToRadians (float degrees)
		{
			return (float)(degrees * Math.PI / 180);
		}
		
		
		
		
		public void StartApplication() 
		{
			this.AppStartFlag = false;

			this.InvokeOnMainThread(delegate {
				this.splashView.RemoveFromSuperview();
			});
		}
		
		
		
		
		public void ShowStockDetail(StockItem item, UIImage image) 
		{
			navigationController.PushViewController(new UICatalogueItemViewController(item, _stockService, image), true);
		}
		
		
		
		
		public void ShowSelectedExtraThumb(UIImage image, int index)
		{
			
			UICatalogueItemViewController itemViewController = navigationController.TopViewController as UICatalogueItemViewController;
			if (itemViewController != null)
			{
				itemViewController.ChangeLargeImage(image, index);
			}//end if
			
		}//end void ShowSelectedExtraThumb
		
		
		
		
		
		public void ShowCatalogue()
		{
			navigationController.PopViewControllerAnimated(true);
		}
		
		#endregion Public methods

		//
		// This method is invoked when the application has loaded and is ready to run. In this 
		// method you should instantiate the window, load the UI into it and then make the window
		// visible.
		//
		// You have 17 seconds to return from this method, or iOS will terminate your application.
		//
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{

			this.CurrencyHelper = new CurrencyHelper();
			AppDelegate.Self = this;
			this.AppStartFlag = true;
			this.referenceDate = new DateTime(1970, 1, 1, 0, 0, 0);
			
//			if (this.IsFirstRun)
//			{
//				this.IsFirstRun = true;
//				this.LastRefreshedDate = DateTime.Now.Subtract(TimeSpan.FromHours(24d));
//			}//end if
			this.LastRefreshedDate = DateTime.Now.Subtract(TimeSpan.FromHours(24d));


			this.MainScreenBounds = UIScreen.MainScreen.Bounds;

			// create a new window instance based on the screen size
			window = new UIWindow (this.MainScreenBounds);
//			this.navigationController = new UINavigationController(new UICatalogueController());

			UIViewController catalogueController = null;
			if (this.IsOldIOS)
			{
				catalogueController = new UICatalogueController();
			} else
			{
				catalogueController = new UICatalogueControllerEx();
			}//end if else

			this.navigationController = new MainNavController(catalogueController);

			window.RootViewController = this.navigationController;

			RectangleF splashViewFrame = window.Bounds;
			
			if (this.DeviceType == DeviceType.iPad)
			{
				
				UIApplication.SharedApplication.SetStatusBarOrientation(UIInterfaceOrientation.LandscapeLeft, true);
				splashViewFrame.X = 10f;
				this.LastInterfaceOrientation = UIInterfaceOrientation.LandscapeLeft;
				
			} else
			{
				
				this.navigationController.NavigationBar.BarStyle = UIBarStyle.Black;
				this.LastInterfaceOrientation = UIInterfaceOrientation.Portrait;
				
			}//end if else

			this.splashView = new UISplashView(splashViewFrame);
			this.splashView.ContentMode = UIViewContentMode.Center;
			
			UIImage splashImage = null;
			if (this.DeviceType == DeviceType.iPad)
			{
				splashImage = UIImage.FromBundle("Default-Landscape");
			} else
			{
				if (UIScreen.MainScreen.Bounds.Height > 480)
				{
					splashImage = UIImage.FromBundle("Default-568h");
				} else
				{
					splashImage = UIImage.FromBundle("Default");
				}//end if else
			}//end if else
			
			this.splashView.Image = splashImage;
			splashImage.Dispose();
			this.splashView.Setup();
			//NOTE: Rotation transformation removed.
			if (this.DeviceType == DeviceType.iPad)
			{
				this.splashView.Transform = CGAffineTransform.MakeRotation(this.DegreesToRadians(-90));
			}//end if
			//this.navigationController.View.AddSubview(this.splashView);

			ThreadPool.QueueUserWorkItem(delegate {
				this.InvokeOnMainThread(delegate {
					window.AddSubview(this.splashView);
				});
			});
			
			// make the window visible
			window.MakeKeyAndVisible ();
			
			return true;
		}
	
	}
}

