// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using MonoTouch.CoreLocation;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Json;
using System.Globalization;

namespace SymbolicChase
{
	public class CurrencyHelper : IDisposable
	{

		#region Constructors

		public CurrencyHelper ()
		{

			this.unixTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
			this.Initialize();

		}

		#endregion Constructors



		#region Constants

		private const string KeyLastGeocodingDate = "LastGeocodingDate";
		private const string KeyCurrencyCode = "CurrencyCode";
		private const string KeyCurrencySymbol = "CurrencySymbol";
		private const string KeyLastExchangeRates = "LastExchangeRates";
		private const string KeyLocationAuthStatus = "LocationAuthStatus";
		private const string KeyDontShowLocationFailureAlert = "DontShowLocationFailureAlert";
		private const int MAX_UPDATE_COUNT = 2;
		private const int MAX_UPDATE_INTERVAL_MINUTES = 5;

		#endregion Constants



		#region Fields

		private CLLocationManager locationManager;
		private CLGeocoder geocoder;
		private TaskCompletionSource<string> currencyCodeTCS;
		private readonly DateTime unixTime;
		private int updateCount;
		private readonly string ExchangeURL = "http://openexchangerates.org/api/latest.json?app_id=579d6595a6a144cf885c225c10eb8d8e";

		#endregion Fields



		#region Properties

		public bool LocationServicesEnabled
		{
			get
			{
				return CLLocationManager.LocationServicesEnabled;
			}//end get

		}//end bool LocationServicesEnabled



		public CLAuthorizationStatus LocationAuthStatus
		{
			get
			{
				CLAuthorizationStatus authStatus = 
					(CLAuthorizationStatus)NSUserDefaults.StandardUserDefaults.IntForKey(KeyLocationAuthStatus);

				return authStatus;

			} private set
			{

				NSUserDefaults.StandardUserDefaults.SetInt((int)value, KeyLocationAuthStatus);

			}//end get private set

		}//end CLAuthorizationStatus LocationAuthStatus





		public bool DontShowLocationFailureAlert
		{
			get
			{
				return NSUserDefaults.StandardUserDefaults.BoolForKey(KeyDontShowLocationFailureAlert);
			} private set
			{
				NSUserDefaults.StandardUserDefaults.SetBool(value, KeyDontShowLocationFailureAlert);
			}//end private set

		}//end bool DontShowLocationFailureAlert





		public DateTime? LastGeocodingDate
		{
			get
			{

				double seconds = NSUserDefaults.StandardUserDefaults.DoubleForKey(KeyLastGeocodingDate);
				if (seconds == 0)
				{
					return null;
				} else
				{
					return this.unixTime.AddSeconds((double)seconds);
				}//end if else

			} private set
			{

				if (value.HasValue)
				{
					TimeSpan ts = value.Value.Subtract(this.unixTime);
					double seconds = ts.TotalSeconds;
					NSUserDefaults.StandardUserDefaults.SetDouble(seconds, KeyLastGeocodingDate);
				} else
				{
					NSUserDefaults.StandardUserDefaults.RemoveObject(KeyLastGeocodingDate);
				}//end if else

			}//end get private set
		}



		public CurrencyInfo LastGeocodedCurrencyCode
		{
			get
			{

				string currencyCode = 
					NSUserDefaults.StandardUserDefaults.StringForKey(KeyCurrencyCode);
				string currencySymbol =
					NSUserDefaults.StandardUserDefaults.StringForKey(KeyCurrencySymbol);

				if (string.IsNullOrEmpty(currencyCode) ||
				    string.IsNullOrEmpty(currencySymbol))
				{
					return null;
				} else
				{
					return new CurrencyInfo(currencyCode, currencySymbol);
				}//end if else

			} private set
			{
				if (null != value)
				{
					NSUserDefaults.StandardUserDefaults.SetString(value.CurrencyCode, KeyCurrencyCode);
					NSUserDefaults.StandardUserDefaults.SetString(value.CurrencySymbol, KeyCurrencySymbol);
				} else
				{
					NSUserDefaults.StandardUserDefaults.RemoveObject(KeyCurrencyCode);
					NSUserDefaults.StandardUserDefaults.RemoveObject(KeyCurrencySymbol);
				}//end if else
			}//end get private set

		}//end string LastGeocodedCurrencyCode




		public string LastExchangeRates
		{
			get
			{
				string toReturn = 
					NSUserDefaults.StandardUserDefaults.StringForKey(KeyLastExchangeRates);
				if (string.IsNullOrEmpty(toReturn))
				{
					return null;
				} else
				{
					return toReturn;
				}//end if else

			} private set
			{

				if (string.IsNullOrEmpty(value))
				{
					NSUserDefaults.StandardUserDefaults.RemoveObject(KeyLastExchangeRates);
				} else
				{
					NSUserDefaults.StandardUserDefaults.SetString(value, KeyLastExchangeRates);
				}//end if else

			}//end get private set

		}//end strin LastExchangeRates

		#endregion Properties



		#region Private methods

		private void Initialize()
		{

			this.locationManager = new CLLocationManager();
			this.locationManager.AuthorizationChanged += LocationManager_AuthorizationChanged;
			this.locationManager.Failed += LocationManager_Failed;
			this.locationManager.LocationsUpdated += LocationManager_LocationsUpdated;

			this.geocoder = new CLGeocoder();

		}//end void Initialize




		private void LocationManager_LocationsUpdated (object sender, CLLocationsUpdatedEventArgs e)
		{

			if (++this.updateCount == MAX_UPDATE_COUNT)
			{

				this.updateCount = 0;
				if (null != e.Locations &&
				    e.Locations.Length > 0)
				{
					CLLocation location = e.Locations[0];
					this.geocoder.ReverseGeocodeLocation(location, this.Geocoder_Completed);

				} else
				{
					this.currencyCodeTCS.TrySetException(new CurrencyException("Did not get any location updates!", ErrorType.NoLocation));
				}//end if else

				this.locationManager.StopUpdatingLocation();

			}//end if
		}





		private void LocationManager_Failed (object sender, NSErrorEventArgs e)
		{
			if (null != this.currencyCodeTCS)
			{
				this.currencyCodeTCS.TrySetException(new CurrencyException(string.Format("Location manager failed: {0}", e.Error.LocalizedDescription),
				                                  ErrorType.LocationManagerFailed));
			}//end if

		}





		private void LocationManager_AuthorizationChanged (object sender, CLAuthorizationChangedEventArgs e)
		{
#if(DEBUG)
			Console.WriteLine("Authorization changed! {0}", e.Status);
#endif

			if (this.LocationAuthStatus != e.Status)
			{

				if (e.Status == CLAuthorizationStatus.Denied &&
				    !this.DontShowLocationFailureAlert)
				{

					UIAlertView alertView = new UIAlertView();
					alertView.AddButton("Don't show again");
					alertView.AddButton("OK");
					alertView.CancelButtonIndex = 0;
					alertView.Title = "Location Services";
					alertView.Message = "App does not have permission to use location services. Prices will be displayed in GBP.";

					EventHandler<UIButtonEventArgs> handler = null;
					handler = (s, args) => {

						alertView.Dismissed -= handler;
						if (args.ButtonIndex == 0)
						{
							this.DontShowLocationFailureAlert = true;
						}//end if

						alertView.Dispose();

					};

					alertView.Dismissed += handler;

					alertView.Show();

				}//end if else

				this.LocationAuthStatus = e.Status;

			}//end if

		}




		private async void Geocoder_Completed(CLPlacemark[] placemarks, NSError error)
		{

			if (null == error)
			{
				string ISOCountryCode = placemarks[0].IsoCountryCode;

				try
				{

					RegionInfo regionInfo = new RegionInfo(ISOCountryCode);
					this.LastGeocodedCurrencyCode = new CurrencyInfo(regionInfo.ISOCurrencySymbol, regionInfo.CurrencySymbol);

					string response = await this.GetCurrencyResponse();
					this.LastExchangeRates = response;
					this.LastGeocodingDate = DateTime.Now;

					double gbp = (double)this.currencyCodeTCS.Task.AsyncState;
					double targetAmount = this.ConvertGBPTo(gbp, this.LastGeocodedCurrencyCode.CurrencyCode, response);

					this.currencyCodeTCS.TrySetResult(this.CreatePriceString(targetAmount, this.LastGeocodedCurrencyCode));

				} catch (Exception ex)
				{
					this.currencyCodeTCS.TrySetException(new CurrencyException(ex.Message, ErrorType.ConnectionError));
				}//end try catch


			} else
			{
				this.currencyCodeTCS.TrySetException(new CurrencyException(string.Format("Geocoding error: {0}", error.LocalizedDescription), 
				                                                        ErrorType.GeocodingError));
			}//end if else

		}//end async void Geocoder_Completed




		private async Task<string> GetCurrencyResponse()
		{

			using (HttpClient httpClient = new HttpClient())
			{

				return await httpClient.GetStringAsync(ExchangeURL);

			}//end using httpClient

		}//end async Task<string> GetCurrencyResponse




		private double ConvertGBPTo(double gbp, string targetCurrency, string jsonResponse)
		{

			JsonValue jsonObj = JsonValue.Parse(jsonResponse);
			JsonValue rates = jsonObj["rates"];
			double gbpToUSD = gbp / (double)rates["GBP"];
			return gbpToUSD * (double)rates[targetCurrency];

		}//end double ConvertGBPTo



		private string CreatePriceString(double targetAmount, CurrencyInfo currencyInfo)
		{

			double rounded = Math.Round(targetAmount, 2);
//			return string.Format("{0} {1}{2}", currencyInfo.CurrencyCode, currencyInfo.CurrencySymbol, rounded);
//			return string.Format("{0} {1:C2}", currencyInfo.CurrencyCode, rounded);
			return string.Format("{0} {1}{2:N2}", currencyInfo.CurrencyCode,
			                     currencyInfo.CurrencySymbol,
			                     rounded);

		}//end string CreatePriceString

		#endregion Private methods




		#region Public methods

		public async Task<string> GetPriceForCurrentLocationAsync(double gbpPrice)
		{

			if (this.LocationServicesEnabled)
			{

				DateTime? lastUpdateDateTime = 
					this.LastGeocodingDate;

				if (!lastUpdateDateTime.HasValue ||
				    DateTime.Now.Subtract(lastUpdateDateTime.Value).TotalMinutes >= MAX_UPDATE_INTERVAL_MINUTES)
				{

					if (null != this.currencyCodeTCS)
					{
						this.currencyCodeTCS = null;
					}//end if
					this.currencyCodeTCS = new TaskCompletionSource<string>(gbpPrice);
					this.locationManager.StartUpdatingLocation();
					return await this.currencyCodeTCS.Task;

				} else
				{
					double targetAmount = this.ConvertGBPTo(gbpPrice, this.LastGeocodedCurrencyCode.CurrencyCode, this.LastExchangeRates);
					return this.CreatePriceString(targetAmount, this.LastGeocodedCurrencyCode);
				}//end if else

			} else
			{

				if (!this.DontShowLocationFailureAlert)
				{

					UIAlertView alertView = new UIAlertView();
					alertView.AddButton("Don't show again");
					alertView.AddButton("OK");
					alertView.Title = "Location Services";
					alertView.Message = "Location services are disabled. Prices will be displayed in GBP.";
					alertView.CancelButtonIndex = 0;

					EventHandler<UIButtonEventArgs> handler = null;
					handler = (s, e) => {

						alertView.Dismissed -= handler;
						if (e.ButtonIndex == 0)
						{
							this.DontShowLocationFailureAlert = true;
						}//end if

						alertView.Dispose();

					};

					alertView.Dismissed += handler;
					alertView.Show();

				}//end if

				throw new CurrencyException("Location services are disabled.", ErrorType.LocationServicesDisabled);

			}//end if else

		}//end async Task<string> GetPriceForCurrentLocationAsync

		#endregion Public methods





		#region IDisposable implementation

		public void Dispose ()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}



		protected void Dispose(bool disposing)
		{

			if (null != this.locationManager)
			{
				this.locationManager.AuthorizationChanged -= this.LocationManager_AuthorizationChanged;
				this.locationManager.LocationsUpdated -= this.LocationManager_LocationsUpdated;
				this.locationManager.Failed -= this.LocationManager_Failed;
			}//end if


			if (disposing)
			{

				if (null != this.locationManager)
				{
					this.locationManager.Dispose();
				}//end if

				if (null != this.geocoder)
				{
					this.geocoder.Dispose();
				}//end if

			}//end if else

		}//end void Dispose



		~CurrencyHelper()
		{
			this.Dispose(false);
		}//end dtor

		#endregion
	}
}

