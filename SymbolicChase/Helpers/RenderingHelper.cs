using System;
using System.Drawing;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using SymboliChase;
using System.Globalization;
using MonoTouch.CoreGraphics;
using System.Threading.Tasks;

namespace SymbolicChase
{
	public static class RenderingHelper
	{
		
		public static float FontSize
		{
			get
			{
				if (AppDelegate.Self.DeviceType == DeviceType.iPad)
				{
					return 25f;
				} else
				{
					return 14f;
				}//end if else
			}//end get
		}//end float FontSize
		
		public const int DescriptionViewTag = 1456;
		public const int ContactUsViewTag = 1457;
		
		/// <summary>
		/// x and y coordinates that are in portrait mode are converted accordingly
		/// </summary>
		/// <param name="x">
		/// A <see cref="System.Single"/>
		/// </param>
		/// <param name="y">
		/// A <see cref="System.Single"/>
		/// </param>
		public static PointF GeneratePoint(float x, float y)
		{
			if (UIDevice.CurrentDevice.Orientation == UIDeviceOrientation.LandscapeLeft || UIDevice.CurrentDevice.Orientation == UIDeviceOrientation.LandscapeRight)
			{
				return new PointF(y, x);
			}
			
			return new PointF(x, y);
		}
		
		
		
		
		public static float SubviewSideMargin
		{
			get
			{
				return AppDelegate.Self.DeviceType == DeviceType.iPad ? 50f : 25f;
			}//end get
		}//end static float SubviewMargin
		
		
		
		#region Obsolete code

//		/// <summary>
//		/// CReate a scaled version of the image within the passed in rectangle
//		/// </summary>
//		/// <param name="image">
//		/// A <see cref="UIImage"/>
//		/// </param>
//		/// <param name="withinRectangle">
//		/// A <see cref="RectangleF"/>
//		/// </param>
//		/// <returns>
//		/// A <see cref="UIImage"/>
//		/// </returns>
//		public static UIImage CreateScaledImageWithinRectangle(UIImage image, RectangleF withinRectangle) 
//		{
//			var widthRatio = 1.0f;
//			var heightRatio = 1.0f;
//
//			widthRatio = image.Size.Width / withinRectangle.Width;
//			heightRatio = image.Size.Height / withinRectangle.Height; 
//			
//			if (heightRatio > widthRatio) 
//			{
//				var newSize = new SizeF(image.Size.Width / heightRatio, image.Size.Height / heightRatio);
//				
//				return image.Scale(newSize); 
//			}
//			
//			var newSize1 = new SizeF(image.Size.Width / widthRatio, image.Size.Height / widthRatio);
//			
//			return image.Scale(newSize1);
//		}

		#endregion Obsolete code



		public static UIImage CreateScaledImageInRect(UIImage image, RectangleF inRect)
		{

			float widthRatio = 1f;
			float heightRatio = 1f;

			widthRatio = image.Size.Width / inRect.Width;
			heightRatio = image.Size.Height / inRect.Height;

			SizeF newSize = SizeF.Empty;
			if (heightRatio > widthRatio)
			{
				newSize = new SizeF(image.Size.Width / heightRatio, image.Size.Height / heightRatio);
			} else
			{
				newSize = new SizeF(image.Size.Width / widthRatio, image.Size.Height / widthRatio);
			}//end if else

			UIImage toReturn = null;
			UIGraphics.BeginImageContextWithOptions(newSize, false, 0f);
			using (CGContext context = UIGraphics.GetCurrentContext())
			{

				context.TranslateCTM(0f, newSize.Height);
				context.ScaleCTM(1f, -1f);
				context.DrawImage(new RectangleF(0f, 0f, newSize.Width, newSize.Height), image.CGImage);

				toReturn = UIGraphics.GetImageFromCurrentImageContext();

			}//end using context
			UIGraphics.EndImageContext();

			return toReturn;

		}//end static UIImage CrateScaledImageInRect




		
		public static UILabel CreateStockHeading(StockItem item, SizeF parentSize)
		{
			var rect = new RectangleF(new PointF(SubviewSideMargin, SubviewSideMargin), new SizeF(parentSize.Width - (SubviewSideMargin * 2), SubviewSideMargin * 2));
			
			var textView = new UILabel 
			{ 
				Text = item.StockTitle.Trim(),
				Frame = rect,
				Font = UIFont.BoldSystemFontOfSize(FontSize),
				TextColor = UIColor.Black,//TextColor = UIColor.FromRGB(0x62, 0x35, 0x35),
				BackgroundColor = UIColor.Clear,
				Tag = 1,
				LineBreakMode = UILineBreakMode.WordWrap,
				Lines = 0
			};
			
			return textView;
		}
		
		
		public static UILabel CreateStockPriceLabel(StockItem item)
		{
			
			UIFont labelFont = UIFont.BoldSystemFontOfSize(FontSize);
			NSString labelText = new NSString(item.ItemPrice.ToString("C0", CultureInfo.CreateSpecificCulture("en-GB")));
			SizeF textSize = labelText.StringSize(labelFont);
			
			RectangleF frame = new RectangleF(new PointF(SubviewSideMargin, SubviewSideMargin * 3), new SizeF(textSize.Width * 4f, SubviewSideMargin));

			PriceLabel priceLabel = new PriceLabel(frame, item.ItemPrice) {

				Font = labelFont,
				BackgroundColor = UIColor.Clear,
				TextAlignment = UITextAlignment.Left

			};

#if(DEBUG)
			Console.WriteLine("GBP PRICE: {0}", labelText);
#endif
			
			return priceLabel;
			
		}//end static UILabel CreateStockPriceLabel
		
		
		
		public static UIContactUsLabel CreateContactUsLabel(StockItem item)
		{
			
			UIFont labelFont = UIFont.BoldSystemFontOfSize(FontSize);
			NSString labelText = new NSString("Contact us!");
			SizeF textSize = labelText.StringSize(labelFont);
			
			RectangleF frame = new RectangleF(new PointF(SubviewSideMargin, SubviewSideMargin * 3), new SizeF(textSize.Width + 10f, SubviewSideMargin));
			
			UIContactUsLabel textView = new UIContactUsLabel(frame, "example@example.com", item) {
				Text = labelText.ToString(),
				Font = labelFont,
				BackgroundColor = UIColor.Clear,
				TextColor = UIColor.Blue,
				TextAlignment = UITextAlignment.Left
			};
			
			return textView;		
		}
		
		public static UILabel CreateStockDescription(StockItem item, SizeF parentSize)
		{
			var rect = new RectangleF(new PointF(SubviewSideMargin, SubviewSideMargin * 4), new SizeF(parentSize.Width - (SubviewSideMargin * 2), parentSize.Height));
			
			var maximumSize = rect.Size;
			
			var textView = new UILabel 
			{
				Text = item.StockDescription.Trim(), 
				Frame = rect,
				Font = UIFont.SystemFontOfSize(FontSize),
				BackgroundColor = UIColor.Clear,
				Tag = 2,
				LineBreakMode = UILineBreakMode.WordWrap,
				Lines = 0,
				ContentMode = UIViewContentMode.Top
			};
			
			var sizeThatFits = textView.SizeThatFits(maximumSize);
			
			var frame = textView.Frame;
			
			frame.Size = sizeThatFits;
			
			textView.Frame = frame;
			
			return textView;
		}
		
		
		#region New CreateStockDescriptionView method
		
		public static UIItemDescriptionView CreateStockDescriptionView(StockItem item)
		{
			UIItemDescriptionView mainView = new UIItemDescriptionView(ScrollViewHelper.CreateScrollFrame());
			mainView.Tag = DescriptionViewTag;
			
			var headingView = RenderingHelper.CreateStockHeading(item, mainView.Bounds.Size);
			var priceView = RenderingHelper.CreateStockPriceLabel(item);
			//var contactUsView = RenderingHelper.CreateContactUsLabel(item);
			var detailView = RenderingHelper.CreateStockDescription(item, mainView.Bounds.Size);
			
			mainView.AddSubview(headingView);
			mainView.AddSubview(priceView);
			//mainView.AddSubview(contactUsView);
			mainView.AddSubview(detailView);
			
			return mainView;
		}
		
		#endregion New CreateStockDescriptionView method
	}
}