using System;
using MonoTouch.Foundation;
using System.Net;
using System.IO;
using System.Runtime.InteropServices;
using System.Collections.Generic;

namespace SymbolicChase
{
	public static class ImageHelper
	{
		public static byte[] GetImageBytes(string url)
		{
			List<byte> bytes = new List<byte>();
			
			var request = WebRequest.Create (url);
            request.Method = "GET";

			using (var response = request.GetResponse())
			{
				using (var sr = new BinaryReader(response.GetResponseStream()))
				{
                	var b = sr.ReadBytes(1024);
					
					while (b.Length > 0) {
						bytes.AddRange(b);
						
						b = sr.ReadBytes(1024);
					}
			    }
			}
			
			return bytes.ToArray();
    	}
	}
}