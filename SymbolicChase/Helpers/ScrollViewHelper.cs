using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.UIKit;
using SymbolicChase;

namespace SymboliChase
{
	
	public enum DeviceOrientation
	{
		Portrait,
		Landscape
	}//end enum DeviceOrientation
	
	public class ScrollViewHelper
	{
		private const float _screenHeight = 748;
		private const float _screenWidth = 1024;
		private const float _screenHeightPhone = 300;
		private const float _screenWidthPhone = 480;
		private const float _screenHeightPhoneTallPortrait = 548f;
		private const float _screenWidthPhoneTall = 568f;
		private const float _screenHeightPortrait = 1004;
		private const float _screenWidthPortrait = 768;
		private const float _screenHeightPhonePortrait = 460;
		private const float _screenWidthPhonePortrait = 320;
		
		private const int _rowCount = 5 ;
		private const int _columnCount = 7 ;
		private const int _rowCountPhone = 2;
		private const int _columnCountPhone = 3;
		private const int _rowCountPortrait = 7;
		private const int _columnCountPortrait = 5;
		private const int _rowCountPortraitPhone = 3;
		private const int _columnCountPortraitPhone = 2;
		
		public const float CellPadding = 2f;
		
		public const float TopOffset = 0f;
		public const float BottomOffset = 50f;
				
//		private static StockRepositoryEx _stockRepository = new StockRepositoryEx() ;
			
		public static List<StockItem> GetPagedStock(int page)
		{
//			var stock = _stockRepository.GetStock(page, PageSize);
//			
//			return stock;
			var stock = AppDelegate.Self.StockService.GetStock(page, PageSize);
			return stock;
		}
	
		/// <summary>
		/// Return the page dependant on the number of columns and rows on the frame
		/// </summary>
		public static int PageSize 
		{
			get 
			{
				
				if (AppDelegate.Self.DeviceType == DeviceType.iPad)
				{
					return _rowCount * _columnCount;
				} else
				{
					return _rowCountPhone * _columnCountPhone;
				}//end if else
			}	
		}
		
		public static DeviceOrientation Orientation
		{
			get
			{
				
				if (AppDelegate.Self.AppStartFlag)
				{
					
					if (AppDelegate.Self.DeviceType == DeviceType.iPad)
					{
						return DeviceOrientation.Landscape;
					} else
					{
						return DeviceOrientation.Portrait;
					}//end if else
					
				} else
				{
					
					return AppDelegate.Self.LastInterfaceOrientation == UIInterfaceOrientation.Portrait ||
					    AppDelegate.Self.LastInterfaceOrientation == UIInterfaceOrientation.PortraitUpsideDown ?
					    DeviceOrientation.Portrait :
					    DeviceOrientation.Landscape;
					
				}//end if else
					
			}//end get
			
		}//end static DeviceOrientation Orientation
		
		public static float ScreenHeight
		{
			
			get
			{
				
				if (AppDelegate.Self.DeviceType == DeviceType.iPad)
				{
					return Orientation == DeviceOrientation.Landscape ?
						_screenHeight :
							_screenHeightPortrait;
				} else
				{
					if (AppDelegate.Self.MainScreenBounds.Height > 480f)
					{

						return Orientation == DeviceOrientation.Landscape ?
							_screenHeightPhone :
								_screenHeightPhoneTallPortrait;

					} else
					{

						return Orientation == DeviceOrientation.Landscape ?
							_screenHeightPhone :
								_screenHeightPhonePortrait;

					}//end if else
				}//end if else
			}//end get
			
		}//end static float ScreenHeight
		
		public static float ScreenWidth
		{
			get
			{
				
				if (AppDelegate.Self.DeviceType == DeviceType.iPad)
				{
					return Orientation == DeviceOrientation.Landscape ?
						_screenWidth :
							_screenWidthPortrait;
				} else
				{

					if (AppDelegate.Self.MainScreenBounds.Height > 480f)
					{

						return Orientation == DeviceOrientation.Landscape ?
							_screenWidthPhoneTall :
								_screenWidthPhonePortrait;
					} else
					{

						return Orientation == DeviceOrientation.Landscape ?
							_screenWidthPhone :
								_screenWidthPhonePortrait;

					}//end if else
				}//end if else
			}//end get
			
		}//end static float ScreenWidth
		
		public static int RowCount
		{
			get
			{
				if (AppDelegate.Self.DeviceType == DeviceType.iPad)
				{
					return Orientation == DeviceOrientation.Landscape ?
						_rowCount :
							_rowCountPortrait;
				} else
				{
					return Orientation == DeviceOrientation.Landscape ?
						_rowCountPhone :
							_rowCountPortraitPhone;
				}//end if else
			}//end get
			
		}//end static int RowCount
		
		public static int ColumnCount
		{
			get
			{
				if (AppDelegate.Self.DeviceType == DeviceType.iPad)
				{
					return Orientation == DeviceOrientation.Landscape ?
						_columnCount :
							_columnCountPortrait;
				} else
				{
					return Orientation == DeviceOrientation.Landscape ?
						_columnCountPhone :
							_columnCountPortraitPhone;
				}//end if else
			}//end get
			
		}//end static int ColumnCount
		
		/// <summary>
		/// Returns a list of Points on the screen where images will go
		/// </summary>
		public static List<RectangleF> ImageMap(UIView view)
		{
			var myList = new List<RectangleF>();
				
			var actualScreenHeight = _screenHeight  - TopOffset - BottomOffset;
			
			var tileWidth = _screenWidth / _columnCount;
			var tileHeight = actualScreenHeight / _rowCount;
			
			for(var yIndex = 0; yIndex < _rowCount; yIndex ++) 
			{
				for(var xIndex = 0; xIndex < _columnCount; xIndex ++) 
				{
					var xCoordinate = xIndex * tileWidth;
					var yCoordinate = yIndex * tileHeight;
					
					var rectangle = new RectangleF(xCoordinate + CellPadding, yCoordinate + CellPadding, tileWidth - (CellPadding * 2), tileHeight - (CellPadding * 2));
					
					myList.Add(rectangle);
				}
			}

			return myList;
		}
		
		
		
		public static List<RectangleF> ImageMapFromOrientation(UIView view)
		{
			
			var myList = new List<RectangleF>();
			var actualScreenHeight = ScreenHeight - TopOffset - BottomOffset;
			
			var titleWidth = ScreenWidth / ColumnCount;
			var titleHeight = actualScreenHeight / RowCount;
			
			for (var yIndex = 0; yIndex < RowCount; yIndex++)
			{
				for (var xIndex = 0; xIndex < ColumnCount; xIndex++)
				{
					var xCoordinate = xIndex * titleWidth;
					var yCoordinate = yIndex * titleHeight;
					
					var rectangle = new RectangleF(xCoordinate + CellPadding, yCoordinate + CellPadding, titleWidth - (CellPadding * 2) / 2f, titleHeight - (CellPadding * 2));
					
					myList.Add(rectangle);
					
				}//end for
				
			}//end for
			
			return myList;
			
		}//end static List<RectangleF> ImageMapFromOrientation
		
		public static RectangleF CreateScrollFrame() 
		{
			return new RectangleF(0, 0, ScreenWidth, ScreenHeight - TopOffset - BottomOffset);
		}
		
		/// <summary>
		/// Return the number of frames there would be
		/// </summary>
		public static int FrameCount 
		{
			get 
			{
//				var count = _stockRepository.GetStockCount();
				var count = AppDelegate.Self.StockService.GetStockCount();
				
				return count % PageSize == 0?count/PageSize:(count/PageSize) + 1;
			}
		}
	}
}