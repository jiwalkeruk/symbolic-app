using System;
using System.Threading;
using System.Xml.Linq;
using MonoTouch.Foundation;
using System.Diagnostics;

namespace SymbolicChase
{
	
	public delegate void FeedInitialisationCompletedHandler(FeedManager sender);
	public class FeedManager : IFeedService
	{
		private static string FeedUrlHost = "rss.symbolicchase.com";
		private static string FeedUrl = "http://rss.symbolicchase.com/CurrentCatalogue.ashx";
		
		private static FeedManager _instance; 
		private readonly Thread _thread ;
		
		private static object mutex = new object();
		
		private static MyThreadStateClass _state = new MyThreadStateClass(FeedStatus.Stopped);
		
		private IStockService _stockService;
		
		public event FeedInitialisationCompletedHandler FeedInitialisationCompleted;
			
		private FeedManager (IStockService service)
		{
			_stockService = service;
			
			_thread = new Thread(DoWork);
		}
		
		/// <summary>
		/// It is up to the caller to check the status of the Manager first to get the stock data
		/// </summary>
		/// <returns>
		/// A <see cref="System.Int32"/>
		/// </returns>
		public int GetStockCount() 
		{
			return _stockService.GetStockCount();
		}
		
		public FeedStatus GetFeedStatus() 
		{
			return _state.State;
		}
		
		public static FeedManager GetInstance(IStockService service) {
			lock (mutex) 
			{
				if (_instance == null) 
				{
					_instance = new FeedManager(service);
				}
				
				return _instance;
			}
		}
		
		public static bool IsFeedConnnected 
		{
			get
			{
				//check connection
				return ReachAbility.IsHostReachable(FeedUrlHost);
			}			
		}
			
		/// <summary>
		/// The thread worker for the. This handler will
		/// 1. Update the image thumb for each
		/// </summary>
		private void DoWork() 
		{
			using (NSAutoreleasePool pool = new NSAutoreleasePool())
			{
				while (_state.State == FeedStatus.Running)
				{
					//check connection
					if (!IsFeedConnnected)
					{
						_state.State = FeedStatus.NoConnection;
						break;
					}
				}
			}
		}


		#region Obsolete Initialise method

//		public bool Initialise() 
//		{
//			
//			//check for reachability
//			if (!IsFeedConnnected)
//			{
//				_state.State = FeedStatus.NoConnection;
//				
//				return false;
//			}
//
//			Stopwatch watch = new Stopwatch();
//			watch.Start();
//
//			//firstly lets set all the current cached stock to pending deletion
//			_stockService.SetStockPendingDeletion();
//			watch.Stop();
//
//			Console.WriteLine("SetStockPendingDeletion completed in {0} milliseconds!", watch.ElapsedMilliseconds);
//			watch.Reset();
//						
//			watch.Start();
//
//			//now get the new stock we have on the system
//			var newStock = FeedAdapter.GetFeed(FeedUrl);
//
//			watch.Stop();
//
//			Console.WriteLine("GetFeed completed in {0} milliseconds!", watch.ElapsedMilliseconds);
//			watch.Reset();
//			watch.Start();
//			
//			_stockService.SaveStock(newStock);
//			watch.Stop();
//			Console.WriteLine("SaveStock completed in {0} milliseconds!", watch.ElapsedMilliseconds);
//			watch.Reset();
//
//			watch.Start();
//			_stockService.PurgePendingDeletionStock();
//			watch.Stop();
//			Console.WriteLine("Purge completed in {0} milliseconds!", watch.ElapsedMilliseconds);
//			
//			if (null != this.FeedInitialisationCompleted)
//			{
//				this.FeedInitialisationCompleted(this);
//			}
//			
//			return true;
//		}

		#endregion Obsolete Initialise method



		public bool Initialise() 
		{
			
			//check for reachability
			if (!IsFeedConnnected)
			{
				_state.State = FeedStatus.NoConnection;
				
				return false;
			}
			
			//firstly lets set all the current cached stock to pending deletion
			_stockService.DeleteCache();
			
			//now get the new stock we have on the system
			var newStock = FeedAdapter.GetFeed(FeedUrl);
			
			_stockService.SaveStock(newStock);

			if (null != this.FeedInitialisationCompleted)
			{
				this.FeedInitialisationCompleted(this);
			}
			
			return true;
		}


		
		[Obsolete("Used when separate process for updating images", true)]
		private void Start() 
		{
			if (_state.State == FeedStatus.Stopped)
			{
				try 
				{
					_state.State = FeedStatus.Initialising;

					if (Initialise())
					{
						_thread.Start();	
					}
				} 
				catch
				{
					
				}
			}
		}
		
		public void Stop() 
		{
			if (_state.State == FeedStatus.Running || _state.State == FeedStatus.Initialising)
			{
			    _state.State = FeedStatus.AwaitingStop;
			
				_thread.Join();
			}
		}
		
		public class MyThreadStateClass 
		{
			private object mutex = new object();
			
			public MyThreadStateClass(FeedStatus initialState)
			{
				State = initialState;
			}
			
			private FeedStatus _state;
			
			public FeedStatus State 
			{ 
				get
				{
					lock (mutex)
					{
						return _state;
					}
				}
				
				set
				{
					lock(mutex)
					{
						_state = value;
					}
				}
			}
		}	
	}
	
	public enum FeedStatus
	{
		Initialising,
		Running,
		AwaitingStop,
		Stopped,
		NoConnection
	}
}