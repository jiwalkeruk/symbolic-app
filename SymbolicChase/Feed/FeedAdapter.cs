using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Diagnostics;
using System.Globalization;

namespace SymbolicChase
{
	public class FeedAdapter
	{
		private static string ImageNameSpace = "http://www.symbolicchase.com";
		private static Func<XElement, string, List<string>> GetImageUrlsFunc = delegate(XElement element, string itemName) {
			
			List<XElement> imageElements = element.Elements().Where(s => s.Name.LocalName.Contains(itemName)).ToList();
						
			List<string> imageLinks = imageElements.Select(s => s.Value).ToList();
			return imageLinks;
			
		};
		
		public static List<StockItem> GetFeed(string url) 
		{
			try 
			{
								
				var rssFeed = XDocument.Load(url);
				
				List<StockItem> myFeeds = new List<StockItem>();
				
				foreach (XElement item in rssFeed.Descendants("item"))
				{

					StockItem stockItem = new StockItem
					{
						StockReference = item.Element("guid").Value,
						HighQualityImage = item.Element(XName.Get("imageurlLarge", ImageNameSpace)).Value,
						MediumQualityImage = item.Element(XName.Get("imageurlMedium", ImageNameSpace)).Value,
						LowQualityImage = item.Element(XName.Get("imageurlSmall", ImageNameSpace)).Value,
						HighQualityImageList = GetImageUrlsFunc(item, "_imageurlLarge"),
						MediumQualityImageList = GetImageUrlsFunc(item, "_imageurlMedium"),
						LowQualityImageList = GetImageUrlsFunc(item, "_imageurlSmall"),
						StockTitle = item.Element("title").Value,
						StockDescription = item.Element("description").Value,

//#if(DEBUG)
//						    ItemPrice = 999999d
//#else
//							ItemPrice = Convert.ToDouble(item.Element(XName.Get("gbp", ImageNameSpace)).Value)
//#endif
						ItemPrice = Convert.ToDouble(item.Element(XName.Get("gbp", ImageNameSpace)).Value, CultureInfo.InvariantCulture)
					};
					
					myFeeds.Add(stockItem);
					
				}//end foreach
							
				return myFeeds;
			} 
			catch (Exception ex) 
			{
				Console.Write(ex.Message + " " + ex.StackTrace);
			}
			
			return new List<StockItem>();
		}
	}
}