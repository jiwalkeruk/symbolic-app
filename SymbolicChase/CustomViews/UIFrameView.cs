// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using System.Collections.Generic;
using System.Threading;
using SymboliChase;

namespace SymbolicChase
{
	public class UIFrameView : UIView
	{
		
		#region Constructors
		
		public UIFrameView (IntPtr handle) : base(handle)
		{
		}
		
		
		
		
		public UIFrameView(int frameIndex, RectangleF frame)
		{
			
			this._frameIndex = frameIndex;
			this.Frame = frame;
			
			this.Initialize();
			
		}//end ctor
		
		#endregion Constructors
		
		
		
		
		#region Fields
		
		private int _frameIndex;
		private List<RectangleF> _imageMap;
		
		#endregion Fields



		#region Properties

		public List<UIWebThumbnailView> ThumbViews
		{
			get;
			private set;
		}//end List<UIWebThumbnailView> ThumbViews

		#endregion Properties
		
		
		
		
		#region Private methods
		
		private void Initialize()
		{

			this.ThumbViews = new List<UIWebThumbnailView>();
				
			this._imageMap = ScrollViewHelper.ImageMapFromOrientation(this);
			var stockList = ScrollViewHelper.GetPagedStock(this._frameIndex);
	
			int stockIndex = 0;
			foreach (var stock in stockList)
			{

				UIWebThumbnailView myView;
			
				myView = new UIWebThumbnailView(this._imageMap[stockIndex++], stock, false);
				this.AddSubview(myView);

				this.ThumbViews.Add(myView);
		
			}//end foreach
				
			
		}//end void Initialize
		
		#endregion Private methods
		
		
		
		#region Public methods
		
		public void AdjustThumbsForOrientation(UIInterfaceOrientation forOrientation)
		{
			
			this._imageMap = ScrollViewHelper.ImageMapFromOrientation(this);
			int stockIndex = 0;
			foreach (UIView eachView in this.Subviews)
			{
				
				eachView.Frame = this._imageMap[stockIndex++];
				
			}//end foreach
			
		}//end void AdjustThumbsForOrientation




		public void StartThumbDownload()
		{

			ThreadPool.QueueUserWorkItem(delegate {

				foreach (UIWebThumbnailView eachThumbView in this.ThumbViews)
				{

					if (!eachThumbView.IsDownloading &&
					    !eachThumbView.IsDownloaded)
					{
						eachThumbView.Download();
					}//end if

				}//end foreach

			});

		}//end void StartThumbDownload



		public bool GetIsDownloading()
		{

			bool toReturn = true;
			foreach (UIWebThumbnailView eachThumbView in this.ThumbViews)
			{
				toReturn &= eachThumbView.IsDownloading;
			}//end foreach

			return toReturn;

		}//end bool GetIsDownloading





		public bool GetIsDownloaded()
		{

			bool toReturn = true;
			foreach (UIWebThumbnailView eachThumbView in this.ThumbViews)
			{
				toReturn &= eachThumbView.IsDownloaded;
			}//end foreach

			return toReturn;

		}//end bool GetIsDownloaded
		
		#endregion Public methods
	}
}

