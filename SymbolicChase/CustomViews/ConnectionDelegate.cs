// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace SymbolicChase
{
	public class ConnectionDelegate : NSUrlConnectionDelegate
	{

		#region Constructors

		public ConnectionDelegate (StockRepositoryEx stockService, ThumbItem thumbItem, Action<ThumbItem> finished)
		{
			this.stockService = stockService;
			this.data = new NSMutableData();
			this.thumbItem = thumbItem;
			this.finishedAction = finished;
		}

		#endregion Constructors



		#region Fields

		private StockRepositoryEx stockService;
		private NSMutableData data;
		private ThumbItem thumbItem;
		private Action<ThumbItem> finishedAction;

		#endregion Fields




		#region Overrides

		public override void ReceivedData (NSUrlConnection connection, NSData data)
		{
			Console.WriteLine("Received data for {0}", this.thumbItem);
			this.data.AppendData(data);
		}



		public override void FinishedLoading (NSUrlConnection connection)
		{

			try
			{

				byte[] imgBuffer = this.data.ToArray();
				thumbItem.Thumbnail = RenderingHelper.CreateScaledImageInRect(UIImage.LoadFromData(this.data), new RectangleF(0f, 0f, 130f, 130f));
				thumbItem.IsWorking = false;
				this.stockService.UpdateStockThumb(this.thumbItem.StockReference, imgBuffer);

				if (null != this.finishedAction)
				{

					this.finishedAction(this.thumbItem);
					this.finishedAction = null;

				}//end if

			} catch (Exception ex)
			{

				thumbItem.IsWorking = true;
#if(DEBUG)
				Console.WriteLine("Error downloading thumbnail for {0}: {1}", this.thumbItem.StockReference, ex.Message);
#endif

			}//end try catch
		}

		#endregion Overrides
	}
}

