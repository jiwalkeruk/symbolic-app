// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.UIKit;
using MonoTouch.Foundation;

namespace SymbolicChase
{
	public class ThumbCell : UICollectionViewCell
	{

		#region Constructors

		public ThumbCell () : base()
		{
			this.Initialize();
		}



		public ThumbCell(IntPtr handle) : base(handle)
		{
			this.Initialize();
		}//end ctor

		#endregion Constructors



		#region Fields

		private UIActivityIndicatorView indicatorView;
		private UIImageView imageView;

		#endregion Fields





		#region Private methods

		private void Initialize()
		{

			this.BackgroundColor = UIColor.White;

			this.imageView = new UIImageView(this.Bounds);
			this.imageView.ContentMode = UIViewContentMode.ScaleAspectFit;
			this.imageView.BackgroundColor = UIColor.Clear;

			this.AddSubview(this.imageView);

			this.indicatorView = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray);
			RectangleF indicatorFrame = this.indicatorView.Frame;
			indicatorFrame.X = (this.Bounds.Width / 2f) - (indicatorFrame.Width / 2f);
			indicatorFrame.Y = (this.Bounds.Height / 2f) - (indicatorFrame.Height / 2f);
			this.indicatorView.Frame = indicatorFrame;
			this.indicatorView.HidesWhenStopped = true;

			this.indicatorView.StartAnimating();

			this.AddSubview(this.indicatorView);

		}//end void Initialize

		#endregion Private methods




		#region Public methods

		public void PopulateData(ThumbItem thumbItem)
		{

			if (thumbItem.IsWorking)
			{
				this.indicatorView.StartAnimating();
			} else
			{
				this.imageView.Image = thumbItem.Thumbnail;
				this.indicatorView.StopAnimating();
			}//end if else

		}//end void PopulateData<TDataObject>

		#endregion Public methods




		#region Overrides

		public override void PrepareForReuse ()
		{

			this.indicatorView.StartAnimating();
			this.imageView.Image = null;
		}

		#endregion Overrides
	}
}

