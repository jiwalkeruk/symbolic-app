using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;

namespace SymbolicChase
{
	/// <summary>
	/// Everything is coded with portrait mode in mind
	/// </summary>
	[Register("UIWebImageView")]
	public abstract class UIWebImageView : UIImageView
	{
		protected SizeF _originalSize ;	 
		
		//public NSMutableData ImageData { get; set; }
		public NSMutableData ImageData { get; set; }
		protected UIActivityIndicatorView _indicatorView ;

		public StockItem StockItem { get; private set; }

		private volatile bool pIsDownloaded;
		private volatile bool pIsDownloading;
		public bool IsDownloaded
		{
			get
			{
				return this.pIsDownloaded;
			} set
			{
				this.pIsDownloaded = value;
			}//end get set

		}//end bool IsDownloaded



		public bool IsDownloading
		{
			get
			{
				return this.pIsDownloading;
			} set
			{
				this.pIsDownloading = value;
			}//end get set

		}//end bool IsDownloading



		
		UIWebImageView (IntPtr handle) : base(handle)
		{
			Initialize ();
		}

		[Export("initWithCoder:")]
		public UIWebImageView (NSCoder coder) : base(coder)
		{
			Initialize ();
		}

		void Initialize ()
		{
			//important NOTE: (was UIViewContentMode.Top)
			ContentMode = UIViewContentMode.ScaleAspectFit;
			
			_indicatorView = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray);
			_indicatorView.HidesWhenStopped = true;
			var width  = (Frame.Width-20)/2;
			var height = (Frame.Height-20)/2;
			_indicatorView.Frame = new RectangleF(new PointF(width,height), new Size(20,20));
			_indicatorView.AutoresizingMask = UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleLeftMargin |
				UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleTopMargin;
			UserInteractionEnabled = true;
			
			AddSubview(_indicatorView);
		}

		public UIWebImageView(RectangleF frame){
			Initialize();

			_indicatorView.Frame = new RectangleF (
                		frame.Size.Width/2,
                		frame.Size.Height/2,
                		_indicatorView.Frame.Size.Width,
                		_indicatorView.Frame.Size.Height);

		}

		public UIWebImageView(RectangleF frame, StockItem stock): base (frame) {
			Initialize();
			StockItem = stock;
			Frame = frame;
		}

		public abstract void Download() ;
		public abstract void DownloadExtraImage(int index);
		
		public virtual void RenderImageWithAspectRatio(UIImage image) {
			_indicatorView.StopAnimating();
			
			ImageData = null;

			Image = RenderingHelper.CreateScaledImageInRect(image, Frame);

			_originalSize = new SizeF(Image.Size.Width, Image.Size.Height);
		}
	}
}
