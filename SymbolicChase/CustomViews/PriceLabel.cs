// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Drawing;
using System.Globalization;

namespace SymbolicChase
{
	public class PriceLabel : UILabel
	{

		#region Constructors

		public PriceLabel (RectangleF frame, double gbpPrice) : base(frame)
		{

			this.GBPPrice = gbpPrice;
			this.SetPrice();

		}

		#endregion Constructors



		#region Properties

		public double GBPPrice
		{
			get;
			private set;
		}//end double GBPPrice

		#endregion Properties



		#region Private methods

		private async void SetPrice()
		{

			try
			{

				this.TextColor = UIColor.Gray;
				this.Text = "Retrieving price...";
				this.Text = await AppDelegate.Self.CurrencyHelper.GetPriceForCurrentLocationAsync(this.GBPPrice);

			} catch (Exception ex)
			{
				Console.WriteLine("Error in setting price! {0}--{1}", ex.Message, ex.StackTrace);
				this.Text = new NSString(this.GBPPrice.ToString("C0", CultureInfo.CreateSpecificCulture("en-GB")));

			} finally
			{
				this.TextColor = UIColor.Black;
			}//end try catch finally

		}//end async void SetPrice

		#endregion Private methods
	}
}

