// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.MessageUI;

namespace SymbolicChase
{
	public class UIContactUsLabel : UILabel
	{
		public UIContactUsLabel (IntPtr handle) : base(handle)
		{
			this.Initialize();
		}
		
		public UIContactUsLabel(RectangleF frame, string recipient, StockItem item) : base(frame)
		{
			this.RecipientAddress = recipient;
			this.Item = item;
			this.Initialize();
		}
		
		
		
		private readonly string RecipientAddress;
		private readonly StockItem Item;
		private void Initialize()
		{
			
			this.UserInteractionEnabled = true;
			
		}
		
		
		
		private MFMailComposeViewController mailController;
		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			
			if (MFMailComposeViewController.CanSendMail)
			{
				this.mailController = new MFMailComposeViewController();
				this.mailController.Finished += MailController_Finished;
				this.mailController.SetToRecipients(new string[] { this.RecipientAddress });
				this.mailController.SetSubject(String.Format("{0} {1}", this.Item.StockReference, this.Item.StockTitle));
				this.mailController.SetMessageBody(this.Item.StockDescription, false);
				AppDelegate.Self.NavController.PresentModalViewController(this.mailController, true);
			} else
			{
				using (UIAlertView alertView = new UIAlertView())
				{
					alertView.Title = "Cannot Send Email";
					alertView.Message = "Device cannot send email.";
					alertView.AddButton("OK");
					alertView.Show();
				}
			}//end if else
			
		}

		private void MailController_Finished (object sender, MFComposeResultEventArgs e)
		{
			
			if (e.Result == MFMailComposeResult.Failed)
			{
				
				using (UIAlertView alertView = new UIAlertView())
				{
					alertView.Title = "Problem Sending Email";
					alertView.Message = "Please try again.";
					alertView.AddButton("OK");
					alertView.Show();
				}
				
			}//end if
			
			this.mailController.DismissModalViewControllerAnimated(true);
			
		}
		
		
	}
}

