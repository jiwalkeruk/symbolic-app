// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.ObjCRuntime;

namespace SymbolicChase
{
	public delegate void PinchReceivedHandler (UIItemDescriptionView itemDescView);
	public class UIItemDescriptionView : UIScrollView
	{
		public UIItemDescriptionView (IntPtr handle) : base(handle)
		{
			
			this.Initialize();
			
		}
		
		public UIItemDescriptionView(RectangleF frame) : base(frame)
		{
			
			this.Initialize();
			
		}
		
		public event PinchReceivedHandler PinchReceived;
		
		
		
		
		private void Initialize()
		{
			this.UserInteractionEnabled = true;
			UIPinchGestureRecognizer pinch = new UIPinchGestureRecognizer(this, new Selector("OnPinch"));
			this.AddGestureRecognizer(pinch);
		}
		
		
		
		public override void SubviewAdded (UIView uiview)
		{
			base.SubviewAdded (uiview);
			
			// Adjust the content size
			SizeF contentSize = this.ContentSize;
			RectangleF viewFrame = uiview.Frame;
			
			if (viewFrame.Bottom > this.Frame.Height)
			{
				// Increase the content size and give it some extra room at the bottom.
				contentSize.Height = viewFrame.Bottom + 50f;
				this.EnableVerticalScroll();
			}
			
			this.ContentSize = contentSize;
			
		}
		
		
		
		
		
		private void EnableVerticalScroll()
		{
			this.ScrollEnabled = true;
			this.AlwaysBounceVertical = true;
			this.ShowsVerticalScrollIndicator = true;
		}
		
		
		
		
		private void DisableVerticalScroll()
		{
			this.ScrollEnabled = false;
			this.AlwaysBounceVertical = false;
			this.ShowsVerticalScrollIndicator = false;
		}
		
		
		
		
		[Export("OnPinch")]
		private void OnPinch(UIGestureRecognizer sender)
		{
			UIPinchGestureRecognizer pinch = sender as UIPinchGestureRecognizer;
			
			switch (pinch.State)
			{
				
			case UIGestureRecognizerState.Recognized:
				
				if (null != this.PinchReceived)
				{
					this.PinchReceived(this);
				}
				
				break;
				
			default:
				// do nothing?
				break;
			}
		}
	}
	
}

