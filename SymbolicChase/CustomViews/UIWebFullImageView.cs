using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.CoreGraphics;
using System.Runtime.InteropServices;

namespace SymbolicChase
{
	[Register("UIWebFullImageView")]
	public partial class UIWebFullImageView : UIWebImageView
	{
//		public UIWebFullImageView(RectangleF frame, StockItem stock, byte[] thumbData): base (frame, stock) 
//		{
//			var pinchGesture = new UIPinchGestureRecognizer();
//			pinchGesture.AddTarget(this, new MonoTouch.ObjCRuntime.Selector("PinchSelector"));
//			AddGestureRecognizer(pinchGesture);
//			
//			ImageData = new NSMutableData();
//			
//			ImageData.AppendBytes(thumbData);
//			
//			//RenderImageWithAspectRatio(UIImage.LoadFromData(ImageData));
//			//RenderImageWithAspectRatio(new UIImage(ImageData));
//			
//			UIImage image = new UIImage(ImageData);
//			RenderImageWithAspectRatio(image);
//		}
		
		public UIWebFullImageView(RectangleF frame, StockItem stock, UIImage thumbImage) : base(frame, stock)
		{
			
			this.Initialize();
			
			this.RenderImageWithAspectRatio(thumbImage);
			
		}
		
		private void Initialize()
		{
			var pinchGesture = new UIPinchGestureRecognizer();
			pinchGesture.AddTarget(this, new MonoTouch.ObjCRuntime.Selector("PinchSelector"));
			AddGestureRecognizer(pinchGesture);
			
		}//end void Initialize
		
		
		public override void DownloadExtraImage(int index)
		{
			
			_indicatorView.StartAnimating();
			NSUrlRequest request = new NSUrlRequest(new NSUrl(StockItem.HighQualityImageList[index]));
			new NSUrlConnection(request, new UrlConnectionDelegate(this), true);
			
		}//end DownloadExtraImage
		
		
		public override void Download()
		{
			_indicatorView.StartAnimating();
			
			NSUrlRequest request = new NSUrlRequest(new NSUrl(StockItem.HighQualityImage));

			new NSUrlConnection(request, new UrlConnectionDelegate(this), true);
		}
		
		[MonoTouch.Foundation.Export("PinchSelector")]
		public void Pinch(UIGestureRecognizer sender) 
		{
			var pgr = (UIPinchGestureRecognizer)sender;
			
			switch (pgr.State) 
			{
				case UIGestureRecognizerState.Changed:
					var frame = Superview.Frame;
				
					frame.Width = _originalSize.Width * pgr.Scale;
					frame.Height = _originalSize.Height * pgr.Scale;
					
				break;
				case UIGestureRecognizerState.Cancelled:
					var cframe = Superview.Frame;
					cframe.Size = new SizeF(_originalSize.Width, _originalSize.Height);
					break;
				case UIGestureRecognizerState.Recognized:
					AppDelegate ad = (AppDelegate)UIApplication.SharedApplication.Delegate;
			
					ad.ShowCatalogue();
					break;
			}
		}
	}
}