using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using System.Runtime.InteropServices;

namespace SymbolicChase
{
	[Register("UIWebThumbnailView")]
	public partial class UIWebThumbnailView : UIWebImageView
	{

		private bool disableTouch;


		public UIWebThumbnailView(RectangleF frame, StockItem stock, bool disableTouch): base (frame, stock) 
		{ 

			this.disableTouch = disableTouch;
			this.Image = UIImage.FromBundle("./Images/icon-threedots");
		}
		
		
		public UIActivityIndicatorView ActivityIndicator
		{
			get
			{
				return _indicatorView;
			}//end get
			
		}//end UIAcitivtyIndicatorView ActivityIndicator
		

		
		public override void DownloadExtraImage(int index)
		{
			
			_indicatorView.StartAnimating();
			var request = new NSUrlRequest(new NSUrl(this.StockItem.LowQualityImageList[index]));
			new NSUrlConnection(request, new UrlConnectionDelegate(this), true);
			
		}//end void DownloadExtraThumb()
		
		


		
		 
		public override void Download()
		{
			this.IsDownloading = true;

			var data = AppDelegate.Self.StockService.GetCachedImage(StockItem.StockReference);
			bool updateThumb = false;
			
			if (data != null)
			{
				
				ImageData = new NSMutableData();
				ImageData.AppendBytes(data);
				
				UIImage image = new UIImage(ImageData);
				if (image.Size.Width == 0 || image.Size.Height == 0)
				{
					
					updateThumb = true;
					
				} else
				{
					this.IsDownloading = false;
					RenderImageWithAspectRatio(new UIImage(ImageData));
					
				}//end if else
				
			} else
			{
				
				updateThumb = true;
				
			}//end if else
			
			if (updateThumb)
			{

				this.InvokeOnMainThread(delegate {

					if (null != this.Image)
					{
						this.Image.Dispose();
						this.Image = null;
					}//end if
					_indicatorView.StartAnimating();
					var request = new NSUrlRequest(new NSUrl(StockItem.LowQualityImage));
					
					new NSUrlConnection(request, new UrlConnectionDelegate(this), true);
				});
				
			}//end if
			
		}




		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			
			base.TouchesEnded (touches, evt);
			
			if (!FeedManager.IsFeedConnnected)
			{
				
				using (UIAlertView alertView = new UIAlertView())
				{
					
					alertView.Title = "Network Error";
					alertView.Message = "Internet connection is required to view the item.";
					alertView.AddButton("OK");
					
					alertView.Show();
					
				}//end using alertView
				
				return;
				
			}//end if
			
			if (this.ActivityIndicator.IsAnimating &&
			    this.Superview is UIFrameView)
			{
				
				return;
				
			}//end if
			
			if (!this.disableTouch)
			{
				AppDelegate ad = (AppDelegate)UIApplication.SharedApplication.Delegate;
			
				ad.ShowStockDetail(StockItem, this.Image);
				
			}//end if
		}
	}
}
