// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.CoreAnimation;
using System.Threading;

namespace SymbolicChase
{
	public class UISplashView : UIImageView
	{
		
		#region Constructors
		
		public UISplashView (IntPtr handle) : base(handle)
		{
		}
		
		
		
		public UISplashView(RectangleF frame) : base(frame)
		{
			
		}
		
		#endregion Constructors
		
		
		
		#region Fields
		
		private UIActivityIndicatorView activityView;
		private UIView activityContainer;
		
		#endregion Fields
		
		
		
		
			
		
		#region Public methods
		
		public void Setup()
		{
			
			if (null == this.activityContainer)
			{
				this.activityContainer = new UIView();
				this.activityContainer.BackgroundColor = UIColor.Clear;
				this.activityView = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.WhiteLarge);
				
				RectangleF activityFrame = this.activityView.Frame;
				
				SizeF activityContainerSize = new SizeF(activityFrame.Width + 50f, activityFrame.Height + 50f);
				this.activityContainer.Frame = 
					new RectangleF((this.Frame.Width / 2f) - (activityContainerSize.Width / 2f),
						(this.Frame.Height / 2f) - (activityContainerSize.Height / 2f) + 50f,
						activityContainerSize.Width,
						activityContainerSize.Height);
				this.activityView.Frame = 
					new RectangleF((this.activityContainer.Frame.Width / 2f - activityFrame.Width / 2f), 
						(this.activityContainer.Frame.Height / 2f) - (activityFrame.Height / 2f),
					activityFrame.Width,
					activityFrame.Height);
								
				using (CALayer bgdLayer = new CALayer())
				{
					
					bgdLayer.BackgroundColor = UIColor.Black.CGColor;
					bgdLayer.Opacity = 0.7f;
					bgdLayer.CornerRadius = 20f;
					bgdLayer.Frame = this.activityContainer.Bounds;
					
					this.activityContainer.Layer.InsertSublayer(bgdLayer, 0);
					
				}//end using bgdLayer
				
				this.activityView.StartAnimating();
				
				this.activityContainer.AddSubview(this.activityView);
				
				this.activityContainer.Alpha = 0f;
				this.AddSubview(this.activityContainer);
				
			}//end if
			
		}//end void Setup
		
		
		
		
		public override void SubviewAdded (UIView uiview)
		{
			base.SubviewAdded (uiview);
		
			this.activityContainer.Alpha = 1f;
		}
		
		#endregion
		
	}
}

