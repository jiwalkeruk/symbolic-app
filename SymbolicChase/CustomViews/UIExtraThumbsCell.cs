// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;

namespace SymbolicChase
{
	public class UIExtraThumbsCell : UITableViewCell
	{
		
		#region Constructors
		
		public UIExtraThumbsCell (IntPtr handle) : base(handle)
		{
		}
		
		
		
		public UIExtraThumbsCell(UITableViewCellStyle cellStyle, string identifier) : base(cellStyle, identifier)
		{
			
			this.SelectionStyle = UITableViewCellSelectionStyle.Gray;
			
		}//end ctor
		
		#endregion Constructors
		
		
		
//		public UIWebThumbnailView ThumbView
//		{
//			get
//			{
//				
//				return this.ContentView.Subviews[0] as UIWebThumbnailView;
//				
//			} set
//			{
//				
//				if (this.ContentView.Subviews == null)
//				{
//					
//					this.ContentView.AddSubview(value);
//					
//				} else
//				{
//					
//					this.ContentView.Subviews[0].RemoveFromSuperview();
//					this.ContentView.AddSubview(value);
//					
//				}//end if else
//				
//			}//end get set
//			
//		}//end UIWebThumbnailView ThumbView
	}
}

