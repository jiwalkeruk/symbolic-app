using System;
using System.Collections.Generic;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Threading;
using System.Drawing;

namespace SymbolicChase
{
	[MonoTouch.Foundation.Register("UICustomProgressView")]  
    //public class UICustomProgressView : UIAlertView
	public class UICustomProgressView : UIAlertView
	{
		private UIProgressView _progressView ;
		private float _increments = 0.01f;
		private IStockService _service ;
		private bool setUpComplete = false;  
        
		public UICustomProgressView (IntPtr handle) : base(handle)  
        {  
        }  
  
        [Export("UICustomProgressView:")]  
        public UICustomProgressView (NSCoder coder) : base(coder)  
        {  
        }  
  
  		public UICustomProgressView(IStockService service) 
		{
			_service = service;
		}
			
		public void Start() 
		{
				
			var stock = _service.GetStock();
			
			_increments = 1.0f / stock.Count;
			
			stock.ForEach(s => {
				
				var request = new NSUrlRequest(new NSUrl(s.LowQualityImage));
	
				new NSUrlConnection(request, new ProgressDelegate(this, s), true);
				
			});
					
		}		
		
		public void Show(string title) 
		{
			Title = title;
			Show();
			
			Start();
		}
		
		public void Hide() 
		{
			DismissWithClickedButtonIndex(0, true);
		}	
		
		public void Update() 
		{
			if (_progressView != null)
			{
				InvokeOnMainThread(UpdateProgressBar);	
			}
		}
		
		private void UpdateProgressBar() {  
            _progressView.SetNeedsDisplay();  
            SetNeedsDisplay();  
        } 
		
		public bool IsFinished {
			get
			{
				return 
					_progressView.Progress == 1.0f ||
					(1.0f - _progressView.Progress) < _increments;
			}
		}
		
		public void IncrementProgress() 
		{
			_progressView.Progress += _increments;
			_progressView.SetNeedsDisplay();  
            Update();
		}
		
		public override void Draw (RectangleF rect)  
	    {  
            if (setUpComplete==false) {  
                if (_progressView==null) {  
                    _progressView = new UIProgressView();  
                    _progressView.Style = UIProgressViewStyle.Default;  
                    _progressView.Progress = 0f;  
                }  
                
				_progressView.Frame = new RectangleF(30.0f, rect.Height - 50f, 225.0f, 11f);  
                AddSubview(_progressView);  
                setUpComplete=true;  
				
				BeginAnimations("");
				SetAnimationDuration(0.1);
				CommitAnimations();
		    }  
            
			base.Draw (rect);  
        }  
	}
}

