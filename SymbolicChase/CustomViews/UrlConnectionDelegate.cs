using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

namespace SymbolicChase
{
	public class UrlConnectionDelegate : NSUrlConnectionDelegate
	{
		UIWebImageView _view;
		IStockService _stockService = new StockRepositoryEx();
		NSMutableData _data = new NSMutableData();
		
		public UrlConnectionDelegate(UIWebImageView view){
			_view = view;
		}

		public override void ReceivedData (NSUrlConnection connection, NSData data)
		{
			_data.AppendData(data);	
		}
		
		public override void FinishedLoading (NSUrlConnection connection)
		{
			
			_view.ImageData = _data;
				
			if (_view is UIWebThumbnailView) 
			{

				try
				{
					var bytes = new byte[_view.ImageData.Length];

					_stockService.UpdateStockThumb(_view.StockItem.StockReference, bytes);

				}
				catch (Exception ex)
				{
					Console.WriteLine("Error writing to thumb cache: " + ex.Message);
				}
			}

			using (UIImage image = new UIImage(_view.ImageData))
			{
				_view.IsDownloading = false;
				_view.IsDownloaded = true;
				_view.RenderImageWithAspectRatio(image);
			}//end using image
		}
	}
}