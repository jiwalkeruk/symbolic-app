// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.UIKit;
using MonoTouch.Foundation;

namespace SymbolicChase
{
	public class CatalogueSource : UICollectionViewSource
	{

		#region Constructors

		public CatalogueSource (UICatalogueControllerEx parentController)
		{
			this.parentController = parentController;
		}

		#endregion Constructors



		#region Fields

		private UICatalogueControllerEx parentController;

		#endregion Fields



		#region Overrides

		public override int NumberOfSections (UICollectionView collectionView)
		{
			return 1;
		}



		public override int GetItemsCount (UICollectionView collectionView, int section)
		{
			return this.parentController.Items.Count;
		}



		public override UICollectionViewCell GetCell (UICollectionView collectionView, NSIndexPath indexPath)
		{
			int rowIndex = indexPath.Row;

			ThumbItem currentItem = this.parentController.Items[rowIndex];
			ThumbCell cell = (ThumbCell)collectionView.DequeueReusableCell(this.parentController.CellID, indexPath);

			cell.PopulateData(currentItem);

			return cell;

		}



		public override void ItemSelected (UICollectionView collectionView, NSIndexPath indexPath)
		{

			ThumbItem selectedItem = this.parentController.Items[indexPath.Item];
			this.parentController.ItemSelected(selectedItem);

		}


		#endregion Overrides
	}
}

