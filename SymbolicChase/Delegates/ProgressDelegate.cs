using System;
using MonoTouch.Foundation;
using System.Runtime.InteropServices;
using MonoTouch.UIKit;
using System.Threading;

namespace SymbolicChase
{
	public class ProgressDelegate : NSUrlConnectionDelegate
	{
		UICustomProgressView _view;
		IStockService _stockService = new StockRepositoryEx();
		NSMutableData _data = new NSMutableData();
		StockItem _stock ;
	
		public ProgressDelegate(UICustomProgressView view, StockItem stock){
			_view = view;
			_stock = stock;
		}

		public override void ReceivedData (NSUrlConnection connection, NSData data)
		{

			_data.AppendData(data);	
		}
		
		public override void FinishedLoading (NSUrlConnection connection)
		{
#if(DEBUG)
			Console.WriteLine("finished loading!");
#endif
			try
			{
				var bytes = new byte[_data.Length];
			
				Marshal.Copy(_data.Bytes, bytes, 0, bytes.Length);
				
				_stockService.UpdateStockThumb(_stock.StockReference, bytes);
				
				//TODO: Test
				this.BeginInvokeOnMainThread(delegate {
					
					_view.IncrementProgress();
				
					if (_view.IsFinished) 
					{
						_view.Hide();
						((AppDelegate)UIApplication.SharedApplication.Delegate).StartApplication();
					}
					
				});
            }
			catch (Exception ex)
			{
				Console.WriteLine("Error writing to thumb cache: " + ex.Message);
			}
		}
	}
}

