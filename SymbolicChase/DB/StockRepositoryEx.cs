// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using SQLite;
using System.Collections.Generic;
using System.IO;
using MonoTouch.Foundation;
using System.Linq;
using System.Threading.Tasks;

namespace SymbolicChase
{
	public class StockRepositoryEx : IStockService
	{

		#region Constructors

		public StockRepositoryEx ()
		{
			this.dbLock = new object();
		}

		#endregion Constructors



		#region Fields

		private string pDBPath;
		public const string DatabaseName = "SymbolicStock.db";
		public const string ExtraImagesTable = "ExtraImages";
		public const string DBClauseSyncOFF = "PRAGMA SYNCHRONOUS=OFF;";
		public const string DBClauseVacuum = "VACUUM;";
		private object dbLock;

		#endregion Fields



		#region Properties

		public string DBPath
		{
			get
			{
				if (string.IsNullOrEmpty(this.pDBPath))
				{
					this.pDBPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), DatabaseName);
				}//end if

				return this.pDBPath;

			}//end get

		}//end string DBPath

		#endregion Properties




		#region Private methods



		#endregion Private methods



		#region Public methods



		#endregion Public methods





		#region IStockService implementation

		public List<StockItem> GetStock ()
		{

			lock (this.dbLock)
			{

				using (SQLiteConnection sqlCon = new SQLiteConnection(this.DBPath))
				{

					sqlCon.Execute(DBClauseSyncOFF);

					return sqlCon.Query<StockItem>("SELECT * FROM StockItem");

				}//end using sqlCon

			}//end lock
		}




		public byte[] GetCachedImage (string stockReference)
		{

			lock (this.dbLock)
			{

				using (SQLiteConnection sqlCon = new SQLiteConnection(this.DBPath))
				{

					sqlCon.Execute(DBClauseSyncOFF);

					Console.WriteLine("Stock reference: {0}", stockReference);

					StockItem stockItem = 
						sqlCon.Query<StockItem>("SELECT ThumbImage FROM StockItem WHERE StockReference=? AND ThumbUploadedDate IS NOT NULL AND ThumbUploadedDate > date('now', '-1 day')",
						                        stockReference)
							.FirstOrDefault();

					if (null != stockItem)
					{
						return stockItem.ThumbImage;
					} else
					{
						return null;
					}//end if else

				}//end using sqlCon

			}//end lock
		}




		public List<StockItem> GetStock (int page, int pageSize)
		{

			lock (this.dbLock)
			{

				using (SQLiteConnection sqlCon = new SQLiteConnection(this.DBPath))
				{

					sqlCon.Execute(DBClauseSyncOFF);

					return sqlCon.Query<StockItem>(string.Format("SELECT * FROM StockItem " +
					                                             "LIMIT {0} " +
					                                             "OFFSET {1}", pageSize, page * pageSize));

				}//end using sqlCon

			}//end lock
		}





		public List<string> GetExtraImagesForStock (string stockReference, ImageSizeType sizeType)
		{

			lock (this.dbLock)
			{

				using (SQLiteConnection sqlCon = new SQLiteConnection(this.DBPath))
				{

					sqlCon.Execute(DBClauseSyncOFF);

					List<ExtraImages> imagesList = 
						sqlCon.Query<ExtraImages>("SELECT * FROM ExtraImages WHERE ImageSizeType=? AND StockReference=?",
						                          sizeType, stockReference);

					return imagesList
						.Select(s => s.ImageUrl)
							.ToList();

				}//end using sqlCon

			}//end lock
		}





		public int GetStockCount ()
		{

			lock (this.dbLock)
			{

				using (SQLiteConnection sqlCon = new SQLiteConnection(this.DBPath))
				{

					sqlCon.Execute(DBClauseSyncOFF);

					return sqlCon.Query<StockItem>("SELECT ID FROM StockItem").Count;

				}//end using sqlCon

			}//end lock
		}




		public void ClearStock ()
		{

			lock (this.dbLock)
			{

				using (SQLiteConnection sqlCon = new SQLiteConnection(this.DBPath))
				{

					sqlCon.Execute(DBClauseSyncOFF);

					sqlCon.Execute("DELETE FROM ExtraImages");
					sqlCon.Execute("DELETE FROM StockItem");

				}//end using sqlCon

			}//end lock
		}




		public void SaveStock (List<StockItem> stock)
		{

			lock (this.dbLock)
			{

				Dictionary<string, StockItem> stockList = new Dictionary<string, StockItem>();
				using (SQLiteConnection sqlCon = new SQLiteConnection(this.DBPath))
				{

					sqlCon.Execute(DBClauseSyncOFF);

					stockList = 
						sqlCon.Query<StockItem>("SELECT * FROM StockItem")
							.ToDictionary(s => s.StockReference, s => s);

					sqlCon.BeginTransaction();

					try
					{

						Type stockItemType = typeof(StockItem);
						Type extraImagesType = typeof(ExtraImages);
						foreach (StockItem eachStockItem in stock)
						{

							StockItem existingItem = null;
							if (stockList.TryGetValue(eachStockItem.StockReference, out existingItem))
							{

								sqlCon.Execute("UPDATE StockItem SET " +
									"StockReference=?, " +
									"HighQualityImage=?, " +
									"MediumQualityImage=?, " +
									"LowQualityImage=?, " +
									"StockTitle=?, " +
									"StockDescription=?, " +
									"ItemPrice=?, " +
									"PendingDeletion=? " +
									"WHERE StockReference=?",
								               eachStockItem.StockReference,
								               eachStockItem.HighQualityImage,
								               eachStockItem.MediumQualityImage,
								               eachStockItem.LowQualityImage,
								               eachStockItem.StockTitle,
								               eachStockItem.StockDescription,
								               eachStockItem.ItemPrice,
								               false,
								               eachStockItem.StockReference);


							} else
							{

								sqlCon.Insert(eachStockItem, stockItemType);

							}//end if else

							// Delete extra images
							sqlCon.Execute("DELETE FROM ExtraImages WHERE StockReference=?", eachStockItem.StockReference);

							foreach (string eachItem in eachStockItem.HighQualityImageList)
							{

								ExtraImages extraImage = new ExtraImages() {
									
									StockReference = eachStockItem.StockReference,
									ImageUrl = eachItem,
									ImageSizeType = ImageSizeType.High
									
								};
								
								sqlCon.Insert(extraImage, extraImagesType);

							}//end foreach
							
							foreach (string eachItem in eachStockItem.MediumQualityImageList)
							{
								ExtraImages extraImage = new ExtraImages() {

									StockReference = eachStockItem.StockReference,
									ImageUrl = eachItem,
									ImageSizeType = ImageSizeType.Medium

								};

								sqlCon.Insert(extraImage, extraImagesType);

							}//end foreach

							foreach (string eachItem in eachStockItem.LowQualityImageList)
							{

								ExtraImages extraImage = new ExtraImages() {

									StockReference = eachStockItem.StockReference,
									ImageUrl = eachItem,
									ImageSizeType = ImageSizeType.Low

								};

								sqlCon.Insert(extraImage, extraImagesType);

							}//end foreach

						}//end foreach

						sqlCon.Commit();

					} catch (Exception ex)
					{
						Console.WriteLine("Error in SaveStock! {0}--{1}", ex.Message, ex.StackTrace);
						sqlCon.Rollback();

					}//end try catch

				}//end using sqlCon

			}//end lock
		}




		public void SetupDb()
		{
			
			lock (this.dbLock)
			{

				string oldDB = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "SymbolicStock");
				if (File.Exists(oldDB))
				{
					File.Delete(oldDB);
				}//end if

				using (SQLiteConnection sqlCon = new SQLiteConnection(this.DBPath))
				{
					
					sqlCon.Execute(DBClauseSyncOFF);
					
					sqlCon.CreateTable<StockItem>();
					sqlCon.CreateTable<ExtraImages>();
					
					sqlCon.Execute(DBClauseVacuum);
					
				}//end using sqlCon
				
				if (File.Exists(this.DBPath))
				{
					
					NSError error = NSFileManager.SetSkipBackupAttribute(this.DBPath, true);

#if(DEBUG)
					if (Directory.Exists("/Users/mrmojo/Desktop"))
					{
						File.Copy(this.DBPath, "/Users/mrmojo/Desktop/SymbolicStock.db", true);
					}//end if
#endif
					if (null != error)
					{
						error.Dispose();
					}//end if
					
				}//end if
				
			}//end lock
			
		}//end void SetupDb




		public void SetStockPendingDeletion ()
		{

			lock (this.dbLock)
			{

				using (SQLiteConnection sqlCon = new SQLiteConnection(this.DBPath))
				{

					sqlCon.Execute(DBClauseSyncOFF);

					sqlCon.Execute("UPDATE StockItem SET PendingDeletion=? WHERE PendingDeletion=?", true, false);

				}//end using sqlCon

			}//end lock
		}




		public void DeleteCache ()
		{

			lock (this.dbLock)
			{

				using (SQLiteConnection sqlCon = new SQLiteConnection(this.DBPath))
				{

					sqlCon.Execute(DBClauseSyncOFF);

					sqlCon.Execute("DELETE FROM StockItem;DELETE FROM ExtraImages;");

				}//end using sqlCon

			}//end lock 
		}




		public void PurgePendingDeletionStock ()
		{

			lock (this.dbLock)
			{

				using (SQLiteConnection sqlCon = new SQLiteConnection(this.DBPath))
				{

					sqlCon.Execute(DBClauseSyncOFF);

					sqlCon.Execute("DELETE FROM ExtraImages WHERE EXISTS (SELECT StockReference FROM StockItem WHERE StockReference=ExtraImages.StockReference AND PendingDeletion=?)",
					               true);

					sqlCon.Execute("DELETE FROM StockItem WHERE PendingDeletion=?", true);

				}//end using sqlCon

			}//end lock
		}




		public void UpdateStockThumb (string stockRef, byte[] imageData)
		{

			lock (this.dbLock)
			{

				using (SQLiteConnection sqlCon = new SQLiteConnection(this.DBPath))
				{

					sqlCon.Execute(DBClauseSyncOFF);

					sqlCon.BeginTransaction();

					try
					{

						sqlCon.Execute("UPDATE StockItem SET " +
							"ThumbImage=?, " +
							"ThumbUploadedDate=date('now'), " +
							"PendingDeletion=? " +
							"WHERE StockReference=?",
						               imageData,
						               false,
						               stockRef);

						sqlCon.Commit();

					} catch (Exception ex)
					{

						sqlCon.Rollback();

					}//end try catch

				}//end using sqlCon

			}//end lock
		}




		public bool GetStockHasValidThumb (string stockReference)
		{

			lock (this.dbLock)
			{

				using (SQLiteConnection sqlCon = new SQLiteConnection(this.DBPath))
				{

					sqlCon.Execute(DBClauseSyncOFF);

					StockItem stockItem = 
						sqlCon.Query<StockItem>("SELECT * FROM StockItem WHERE StockReference=? AND ThumbUploadedDate IS NOT NULL AND ThumbUploadedDate > date('now', '-1 day')",
						                        stockReference)
							.FirstOrDefault();

					return stockItem != null;

				}//end using sqlCon

			}//end lock
		}




		public StockItem GetStockItem(string stockReference)
		{

			lock (this.dbLock)
			{

				using (SQLiteConnection sqlCon = new SQLiteConnection(this.DBPath))
				{

					sqlCon.Execute(DBClauseSyncOFF);

					StockItem toReturn = 
						sqlCon.Query<StockItem>("SELECT * FROM StockItem WHERE StockReference=?", stockReference)
							.FirstOrDefault();

					return toReturn;

				}//end using sqlCon

			}//end lock

		}//end StockItem GetStockItem

		#endregion
	}
}

