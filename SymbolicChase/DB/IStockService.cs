using System;
using System.Collections.Generic;

namespace SymbolicChase
{
	public interface IStockService
	{
		List<StockItem> GetStock();
		
		byte[] GetCachedImage(string stockReference);
		
		List<StockItem> GetStock (int page, int pageSize);
		
		List<string> GetExtraImagesForStock(string stockReference, ImageSizeType sizeType);
				
		int GetStockCount();
		
		void ClearStock();
			
		void SaveStock(List<StockItem> stock) ;
		
		void SetupDb();
		
		void SetStockPendingDeletion();

		void DeleteCache();
		
		void PurgePendingDeletionStock();
		
		void UpdateStockThumb(string stockRef, byte[] imageData);
		
		bool GetStockHasValidThumb(string stockReference);
	}

	public enum ImageSizeType
	{
		Low,
		Medium,
		High
		
	}//end enum ImageSizeType
}