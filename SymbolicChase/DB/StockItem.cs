using System.Collections.Generic;
using SQLite;
using System;


namespace SymbolicChase
{
	public class StockItem
	{

		public StockItem()
		{
		}

		[PrimaryKey, AutoIncrement]
		public int ID { get; private set; }

		public string StockReference { get; set; }
		
		public string StockTitle { get ; set ; } 
		
		public string StockDescription { get ; set ; } 
		
		public string LowQualityImage { get; set; }
		
		public string MediumQualityImage { get; set; }

		public string HighQualityImage { get; set; }

		[Ignore]
		public List<string> HighQualityImageList { get; set; }

		[Ignore]
		public List<string> MediumQualityImageList { get; set; }

		[Ignore]
		public List<string> LowQualityImageList { get; set; }
		
		public double ItemPrice { get; set; }

		public bool PendingDeletion { get; set; }

		public byte[] ThumbImage { get; set; }

		public DateTime? ThumbUploadedDate { get; set; }
		
	}




}