using System;
using System.Linq;
using System.Collections.Generic;
using Mono.Data.Sqlite;
using System.IO;
using System.Data;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace SymbolicChase
{
	
		
	public class StockRepository : IStockService
	{
		public const string DatabaseName = "SymbolicStock";
		public const string ExtraImagesTable = "ExtraImages";
		public readonly string DataBasePath ;
		private object dbLock;
		
		public StockRepository()
		{
			DataBasePath = Path.Combine(Environment.GetFolderPath (Environment.SpecialFolder.Personal), DatabaseName); 
			this.dbLock = new object();
		}
		
		/// <summary>
		/// Persists a list of stock in the database. If they already exists the stock is updated
		/// </summary>
		/// <param name="stock">
		/// A <see cref="IEnumerable<StockItem>"/>
		/// </param>
		public void SaveStock(List<StockItem> stock) 
		{

			using (var conn = new SqliteConnection(string.Format("Data Source={0}; Synchronous=OFF;", DataBasePath))) 
			{
				conn.Open();


				SqliteTransaction trans = conn.BeginTransaction(IsolationLevel.ReadCommitted);

				List<string> stockReferenceList = new List<string>();
				using (SqliteCommand sqlCom = new SqliteCommand(conn))
				{
					sqlCom.CommandText = string.Format("SELECT StockReference FROM {0}", DatabaseName);
					using (SqliteDataReader dbReader = sqlCom.ExecuteReader())
					{
						
						if (dbReader.HasRows)
						{
							while (dbReader.Read())
							{
								stockReferenceList.Add((string)dbReader["StockReference"]);
							}//end while
						}//end if
						
					}//end using dbReader
				}//end using
				
				try
				{

		  			foreach(var s in stock)
					{
						
//						if (StockExists(s.StockReference, conn))
						if (stockReferenceList.Contains(s.StockReference))
						{
							//do not update the thumb image or date updated on the thumb image. This will be done by the 
							//images themselves
							using (var cmd = conn.CreateCommand())
							{
								cmd.Parameters.Add(new SqliteParameter("StockReference", s.StockReference));
								cmd.Parameters.Add(new SqliteParameter("HighQualityImage", s.HighQualityImage));
								cmd.Parameters.Add(new SqliteParameter("MediumQualityImage", s.MediumQualityImage));
								cmd.Parameters.Add(new SqliteParameter("LowQualityImage", s.LowQualityImage));
								cmd.Parameters.Add(new SqliteParameter("StockTitle", s.StockTitle));
								cmd.Parameters.Add(new SqliteParameter("StockDescription", s.StockDescription));
								cmd.Parameters.Add(new SqliteParameter("ItemPrice", s.ItemPrice));
								
								cmd.CommandText = string.Format(@"update {0} set  
																StockReference = ?, 
																HighQualityImage = ?,
																MediumQualityImage = ?, 
																LowQualityImage = ?, 
																StockTitle = ?,
																StockDescription = ?,
																ItemPrice = ?,
																PendingDeletion = 0 where StockReference = '{1}'", DatabaseName, s.StockReference);
			
			  					cmd.ExecuteNonQuery();
								
								cmd.Parameters.Clear();
								
								// Refresh the extra images table
								cmd.CommandText = string.Format("DELETE FROM ExtraImages WHERE StockReference = '{0}'", s.StockReference);
								cmd.ExecuteNonQuery();
								
								cmd.Parameters.Add(new SqliteParameter("StockReference", string.Empty));
								cmd.Parameters.Add(new SqliteParameter("ImageSizeType", 0));
								cmd.Parameters.Add(new SqliteParameter("ImageUrl", string.Empty));
								
								cmd.CommandText = string.Format(@"insert into {0}
																(StockReference,
																ImageSizeType,
																ImageUrl) values (?, ?, ?)", ExtraImagesTable);
								
								foreach (string eachItem in s.HighQualityImageList)
								{
									cmd.Parameters["StockReference"].Value = s.StockReference;
									cmd.Parameters["ImageSizeType"].Value = (int)ImageSizeType.High;
									cmd.Parameters["ImageUrl"].Value = eachItem;
									
									cmd.ExecuteNonQuery();
									
								}//end foreach
								
								
								
								foreach (string eachItem in s.MediumQualityImageList)
								{
									cmd.Parameters["StockReference"].Value = s.StockReference;
									cmd.Parameters["ImageSizeType"].Value = (int)ImageSizeType.Medium;
									cmd.Parameters["ImageUrl"].Value = eachItem;
									
									cmd.ExecuteNonQuery();
									
								}//end foreach
								
								
								
								foreach (string eachItem in s.LowQualityImageList)
								{
									cmd.Parameters["StockReference"].Value = s.StockReference;
									cmd.Parameters["ImageSizeType"].Value = (int)ImageSizeType.Low;
									cmd.Parameters["ImageUrl"].Value = eachItem;
									
									cmd.ExecuteNonQuery();
									
								}//end foreach
									
							}
						}
						else
						{
							using (var cmd = conn.CreateCommand())
							{
								cmd.Parameters.Add(new SqliteParameter("StockReference", s.StockReference));
								cmd.Parameters.Add(new SqliteParameter("HighQualityImage", s.HighQualityImage));
								cmd.Parameters.Add(new SqliteParameter("MediumQualityImage", s.MediumQualityImage));
								cmd.Parameters.Add(new SqliteParameter("LowQualityImage", s.LowQualityImage));
								cmd.Parameters.Add(new SqliteParameter("StockTitle", s.StockTitle));
								cmd.Parameters.Add(new SqliteParameter("StockDescription", s.StockDescription));
								cmd.Parameters.Add(new SqliteParameter("StockReference", s.StockReference));
								cmd.Parameters.Add(new SqliteParameter("ItemPrice", s.ItemPrice));
								
								cmd.CommandText = @"insert into SymbolicStock 
																(StockReference, 
																HighQualityImage,
																MediumQualityImage, 
																LowQualityImage, 
																StockTitle,
																StockDescription,
																ItemPrice,
																PendingDeletion) values (?, ?, ?, ?, ?, ?, ?, 0)";
			
			  					cmd.ExecuteNonQuery();
								
								cmd.Parameters.Clear();
								cmd.Parameters.Add(new SqliteParameter("StockReference", string.Empty));
								cmd.Parameters.Add(new SqliteParameter("ImageSizeType", 0));
								cmd.Parameters.Add(new SqliteParameter("ImageUrl", string.Empty));
								
								cmd.CommandText = string.Format(@"insert into {0}
																(StockReference,
																ImageSizeType,
																ImageUrl) values (?, ?, ?)", ExtraImagesTable);
								
								foreach (string eachItem in s.HighQualityImageList)
								{
									cmd.Parameters["StockReference"].Value = s.StockReference;
									cmd.Parameters["ImageSizeType"].Value = (int)ImageSizeType.High;
									cmd.Parameters["ImageUrl"].Value = eachItem;
									
									cmd.ExecuteNonQuery();
									
								}//end foreach
								
								
								
								foreach (string eachItem in s.MediumQualityImageList)
								{
									cmd.Parameters["StockReference"].Value = s.StockReference;
									cmd.Parameters["ImageSizeType"].Value = (int)ImageSizeType.Medium;
									cmd.Parameters["ImageUrl"].Value = eachItem;
									
									cmd.ExecuteNonQuery();
									
								}//end foreach
								
								
								
								foreach (string eachItem in s.LowQualityImageList)
								{
									cmd.Parameters["StockReference"].Value = s.StockReference;
									cmd.Parameters["ImageSizeType"].Value = (int)ImageSizeType.Low;
									cmd.Parameters["ImageUrl"].Value = eachItem;
									
									cmd.ExecuteNonQuery();
									
								}//end foreach
								
							}
						}
					}
					
					trans.Commit();
					
				} catch (Exception ex)
				{
					
					trans.Rollback();
#if(DEBUG)
					Console.WriteLine("Exception : {0}", ex.Message + " " + ex.StackTrace);
#endif
					
				} finally 
				{
					if (trans != null)
					{
						trans.Dispose();
					}//end if
					
				}//end try catch finally
			}

		}
		
		
		
		//NOTE: New method
		private bool StockExists(string stockReference, SqliteConnection sqlCon)
		{

			using (var cmd = sqlCon.CreateCommand())
			{
				
				cmd.CommandText = string.Format(@"select 1 from {0} where StockReference = '{1}'", DatabaseName, stockReference);
				
				if (cmd.ExecuteScalar() != null)
				{
					return true;
				}//end if
				
				return false;
				
			}//end using cmd
			
		}
		
		private bool StockExists(string stockReference)
		{

			using (var conn = new SqliteConnection(string.Format("Data Source={0}; Synchronous=OFF;", DataBasePath))) 
			{
				using (var cmd = conn.CreateCommand())
				{
					conn.Open();

  					cmd.CommandText = string.Format(@"select 1 from {0} where StockReference = '{1}'", DatabaseName, stockReference);

  					using(var reader = cmd.ExecuteReader())
					{
						if (reader.Read())
					    {
							return true;
					    }
					}
					
					return false;
				}
			}

		}
		
		private void DeleteStock(string stockReference)
		{

			using (var conn = new SqliteConnection(string.Format("Data Source={0}; Synchronous=OFF;", DataBasePath))) 
			{
				using (var cmd = conn.CreateCommand())
				{
					conn.Open();

  					cmd.CommandText = string.Format(@"delete from SymbolicStock where Stockreference = '{0}'", stockReference);
					
					cmd.ExecuteNonQuery();
					
					cmd.CommandText = string.Format(@"delete from {0} where StockReference = '{1}'", ExtraImagesTable, stockReference);
					
					cmd.ExecuteNonQuery();
				}
			}

		}




		public bool GetStockHasValidThumb(string stockReference) 
		{

			using (var conn = new SqliteConnection(string.Format("Data Source={0}; Synchronous=OFF;", DataBasePath))) 
			{
				using (var cmd = conn.CreateCommand())
				{
					conn.Open();

  					cmd.CommandText = string.Format(@"select 1 from {0} where StockReference = '{1}' AND ThumbUploadedDate IS NOT NULL AND ThumbUploadedDate > date('now','-1 day')", DatabaseName, stockReference);

  					using(var reader = cmd.ExecuteReader())
					{
						if (reader.Read())
					    {
							return true;
					    }
					}
				}
			}

			return false;
		}




		public void PurgePendingDeletionStock()
		{

			using (var conn = new SqliteConnection(string.Format("Data Source={0}; Synchronous=OFF;", DataBasePath))) 
			{
				using (var cmd = conn.CreateCommand())
				{
					conn.Open();
					
					// Delete from the extra images table first
					cmd.CommandText = string.Format(@"delete from {0} where exists (select SymbolicStock.StockReference from SymbolicStock
													where SymbolicStock.StockReference={0}.StockReference and
													SymbolicStock.PendingDeletion = 1)", ExtraImagesTable);
					cmd.ExecuteNonQuery();
					
  					cmd.CommandText = @"delete from SymbolicStock where PendingDeletion = 1";
					
					cmd.ExecuteNonQuery();
				}
			}

		}





		
		/// <summary>
		/// Returns all stock in the database
		/// </summary>
		/// <returns>
		/// A <see cref="IEnumerable<StockItem>"/>
		/// </returns>
		public List<StockItem> GetStock ()
		{

			var stockList = new List<StockItem>();
						
			using (var conn = new SqliteConnection(string.Format("Data Source={0}; Synchronous=OFF;", DataBasePath))) 
			{
				using (var cmd = conn.CreateCommand())
				{
					conn.Open();

  					cmd.CommandText = 
						@"select HighQualityImage, LowQualityImage, MediumQualityImage, StockDescription, StockReference, StockTitle, ItemPrice from SymbolicStock order by StockReference desc";
					
  					using(var reader = cmd.ExecuteReader())
					{
						while (reader.Read())
					    {
									
							var s = new StockItem() 
							{
								HighQualityImage = reader["HighQualityImage"].ToString(),
								LowQualityImage = reader["LowQualityImage"].ToString(),
								MediumQualityImage = reader["MediumQualityImage"].ToString(),
								StockDescription = reader["StockDescription"].ToString(),
								StockReference = reader["StockReference"].ToString(),
								StockTitle = reader["StockTitle"].ToString(),
								ItemPrice = Convert.ToDouble(reader["ItemPrice"])
							};
							
							stockList.Add(s);
					    }
					}
				}
			}
			return stockList;
			
		}




		public StockItem GetStockItem(string stockReference)
		{

			using (SqliteConnection sqlCon = new SqliteConnection(string.Format("Data Source={0}; Synchronous=OFF;", DataBasePath)))
			{
				using (SqliteCommand sqlCom = sqlCon.CreateCommand())
				{
					sqlCon.Open();

					sqlCom.CommandText = 
						string.Format(@"SELECT HighQualityImage, LowQualityImage, MediumQualityImage, StockDescription, StockReference, StockTitle, ItemPrice FROM SymbolicStock WHERE StockReference='{0}'",
						              stockReference);

					using (SqliteDataReader dbReader = sqlCom.ExecuteReader())
					{

						if (dbReader.HasRows)
						{
							dbReader.Read();

							StockItem toReturn = new StockItem() {

								HighQualityImage = dbReader["HighQualityImage"].ToString(),
								LowQualityImage = dbReader["LowQualityImage"].ToString(),
								MediumQualityImage = dbReader["MediumQualityImage"].ToString(),
								StockDescription = dbReader["StockDescription"].ToString(),
								StockReference = dbReader["StockReference"].ToString(),
								StockTitle = dbReader["StockTitle"].ToString(),
								ItemPrice = Convert.ToDouble(dbReader["ItemPrice"])

							};

							return toReturn;

						} else
						{

							return null;

						}//end if else

					}//end using dbReader

				}//end using sqlCom

			}//end using sqlCon

		}//end StockItem GetStockItem



		
		
		
		//NOTE: New method
		public List<string> GetExtraImagesForStock(string stockReference, ImageSizeType sizeType)
		{

			List<string> toReturn = new List<string>();
			using (SqliteConnection sqlCon = new SqliteConnection(string.Format("Data Source={0}; SYNCHRONOUS=OFF;", DataBasePath)))
			{
				using (SqliteCommand sqlCom = sqlCon.CreateCommand())
				{
					sqlCon.Open();
					
					sqlCom.CommandText = string.Format("SELECT ImageUrl FROM ExtraImages WHERE ImageSizeType={0} AND StockReference='{1}'", (int)sizeType, stockReference);
					
					using (SqliteDataReader dbReader = sqlCom.ExecuteReader())
					{
						while (dbReader.Read())
						{
							
							toReturn.Add(Convert.ToString(dbReader["ImageUrl"]));
							
						}//end while
						
					}//end using SqliteDataReader
					
				}//end using sqlCom
				
			}//end using sqlCon
			
			return toReturn;	
			
		}//end Dictionary<string, string> GetExtraImagesForStock
		
		
		
		
		
		public byte[] GetCachedImage(string stockReference) 
		{

			using (var conn = new SqliteConnection(string.Format("Data Source={0}; Synchronous=OFF;", DataBasePath))) 
			{
				using (var cmd = conn.CreateCommand())
				{
					conn.Open();

					cmd.CommandText = string.Format(@"select ThumbImage from {0} where StockReference = '{1}' AND ThumbUploadedDate IS NOT NULL AND ThumbUploadedDate > date('now', '-1 day')", DatabaseName, stockReference);
										
					using (var reader = cmd.ExecuteReader())
					{
						if (reader.HasRows)
						{
							return reader[0] as byte[];
						} else
						{
							return null;
						}//end if else
					}//end using reader

				}
			}
			
		}
		
		
		/// <summary>
		/// Returns paged stock in the database
		/// </summary>
		/// <returns>
		/// A <see cref="IEnumerable<StockItem>"/>
		/// </returns>
//		public List<StockItem> GetStock (int page, int pageSize)
//		{
//			if (this.StockList == null || this.StockList.Count == 0)
//			{
//				this.StockList = this.GetStock().ToDictionary(s => s.StockReference, t => t);
//			}//end if
//			//return GetStock().Skip(page * pageSize).Take(pageSize).ToList();
//			return this.StockList.Select(s => s.Value).Skip(page * pageSize).Take(pageSize).ToList();
//
//		}



		public List<StockItem> GetStock(int page, int pageSize)
		{

			var stockList = new List<StockItem>();
			
			using (var conn = new SqliteConnection(string.Format("Data Source={0}; Synchronous=OFF;", DataBasePath))) 
			{
				using (var cmd = conn.CreateCommand())
				{
					conn.Open();
					
					cmd.CommandText = 
						string.Format("SELECT HighQualityImage, " +
							"LowQualityImage, " +
							"MediumQualityImage, " +
							"StockDescription, " +
							"StockReference, " +
							"StockTitle, " +
							"ItemPrice FROM SymbolicStock LIMIT {0} OFFSET {1} ORDER BY StockReference DESC", pageSize, page * pageSize);
					
					using(var reader = cmd.ExecuteReader())
					{
						while (reader.Read())
						{
							
							var s = new StockItem() 
							{
								HighQualityImage = reader["HighQualityImage"].ToString(),
								LowQualityImage = reader["LowQualityImage"].ToString(),
								MediumQualityImage = reader["MediumQualityImage"].ToString(),
								StockDescription = reader["StockDescription"].ToString(),
								StockReference = reader["StockReference"].ToString(),
								StockTitle = reader["StockTitle"].ToString(),
								ItemPrice = Convert.ToDouble(reader["ItemPrice"])
							};
							
							stockList.Add(s);
						}
					}
				}
			}
			return stockList;

		}//end List<StockItem> GetPagedStock



		
		/// <summary>
		/// Clears all stock form the sqlite database
		/// </summary>
		public void ClearStock ()
		{

			using (var conn = new SqliteConnection(string.Format("Data Source={0}; Synchronous=OFF;", DataBasePath))) 
			{
				using (var cmd = conn.CreateCommand())
				{
					conn.Open();

  					cmd.CommandText = @"delete from SymbolicStock";
					
					cmd.ExecuteNonQuery();
					
					cmd.CommandText = @"delete from ExtraImages";
					
					cmd.ExecuteNonQuery();
				}
			}
			this.StockList.Clear();
			this.StockList = null;

		}





		/// <summary>
		/// Returns the count of the number of stock in the database. Sould try to make this better as its takes a count 
		/// by returning all records and counting them. Sqlite connection stuff is not fully understood at this time.
		/// </summary>
		/// <returns>
		/// number of records
		/// </returns>
		public int GetStockCount() 
		{
			if (this.StockList == null || this.StockList.Count == 0)
			{
				this.StockList = this.GetStock().ToDictionary(s => s.StockReference, t => t);
			}//end if
			return this.StockList.Count;
		}




		
		public Dictionary<string, StockItem> StockList
		{
			get;
			private set;
		}



		
		public void SetStockPendingDeletion()
		{

			using (var conn = new SqliteConnection(string.Format("Data Source={0}; Synchronous=OFF;", DataBasePath))) 
			{
				using (var cmd = conn.CreateCommand())
				{
					conn.Open();

  					cmd.CommandText =string.Format(@"update {0} set PendingDeletion = 1 WHERE PendingDeletion = 0", DatabaseName);

  					cmd.ExecuteNonQuery();
				}
			}

		}



		public void DeleteCache()
		{

			using (SqliteConnection sqlCon = new SqliteConnection(string.Format("Data Source={0}; Synchronous=OFF;", DataBasePath)))
			{
				using (SqliteCommand sqlCom = sqlCon.CreateCommand())
				{
					sqlCon.Open();

					sqlCom.CommandText = string.Format("DELETE FROM {0}; DELETE FROM {1}", DatabaseName, ExtraImagesTable);
					sqlCom.ExecuteNonQuery();

				}//end using sqlCom
			}//end using sqlCon

		}//end void DeleteCache



		
		//TODO: Implement transaction
		public void UpdateStockThumb(string stockRef, byte[] imageData)
		{

			using (var conn = new SqliteConnection(string.Format("Data Source={0}; Synchronous=OFF;", DataBasePath))) 
			{
				conn.Open();

  				using (var cmd = conn.CreateCommand())
				{
					cmd.Parameters.Add(new SqliteParameter(DbType.Binary, imageData));
					
					cmd.CommandText = string.Format(@"update {0} set ThumbImage = ?, ThumbUploadedDate = date('now'), PendingDeletion = 0 where StockReference = '{1}'", DatabaseName, stockRef);

  					cmd.ExecuteNonQuery();
				}
			}

		}



		public void SetupDb() 
		{
			if (!File.Exists(DataBasePath)) 
			{
				SqliteConnection.CreateFile (DataBasePath);
				NSError error = NSFileManager.SetSkipBackupAttribute(DataBasePath, true);
				if (null != error)
				{
					error.Dispose();
				}//end if

			}//end if
			
			if (!CheckTableExists()) 
			{
				CreateTableStructure();
			}
			
			if (!CheckFieldExists(DatabaseName, "ItemPrice"))
			{
				
				this.AddFieldToTable(DatabaseName, "ItemPrice", "REAL");
				
			}//end if
		}



		
		private bool DropTable() 
		{

			using (var conn = new SqliteConnection(string.Format("Data Source={0}; Synchronous=OFF;", DataBasePath))) 
			{
				using (var cmd = conn.CreateCommand())
				{
					conn.Open();

  					cmd.CommandText =string.Format(@"drop table " + DatabaseName);

  					cmd.ExecuteNonQuery();
					
					cmd.CommandText = string.Format(@"drop table " + ExtraImagesTable);
					
					cmd.ExecuteNonQuery();
				}
			}
			
			return false;

		}




		private bool CheckTableExists() 
		{

			using (var conn = new SqliteConnection(string.Format("Data Source={0}; Synchronous=OFF;", DataBasePath))) 
			{
				using (var cmd = conn.CreateCommand())
				{
					conn.Open();

  					cmd.CommandText =string.Format(@"select 1 from sqlite_master where name = '{0}'", DatabaseName);

  					using(var reader = cmd.ExecuteReader())
					{
						while (reader.Read())
					    {
							return true;
					    }
					}
				}
			}

			return false;
		}




		private void CreateTableStructure() 
		{
			
#if(DEBUG)
			Console.WriteLine("Database path: {0}", DataBasePath);
#endif

			using (var conn = new SqliteConnection(string.Format("Data Source={0}; Synchronous=OFF;", DataBasePath))) 
			{
				using (var cmd = conn.CreateCommand())
				{
					conn.Open();

  					var commands = new List<string> 
					{
						"BEGIN TRANSACTION",
						string.Format("CREATE TABLE {0} (StockTitle TEXT, StockReference TEXT, StockDescription TEXT, LowQualityImage TEXT, MediumQualityImage TEXT, HighQualityImage TEXT, PendingDeletion BIT, ThumbImage BLOB, ThumbUploadedDate DATETIME, ItemPrice REAL)", DatabaseName),
						string.Format("CREATE TABLE {0} (StockReference TEXT, ImageSizeType INTEGER, ImageUrl TEXT)", ExtraImagesTable),
						"COMMIT"
					};
					
					commands.ForEach(c => {
						cmd.CommandText = c; 
						cmd.ExecuteNonQuery();
					});
				}
			}

		}
		



		private bool CheckFieldExists(string inTable, string field)
		{

			using (var conn = new SqliteConnection(string.Format("Data Source={0}; Synchronous=OFF;", DataBasePath)))
			{
				using (var cmd = conn.CreateCommand())
				{
					conn.Open();
					cmd.CommandText = string.Format(@"select {0} from {1}", field, inTable);
					
					try
					{
						
						cmd.ExecuteScalar();
						return true;
						
					} catch
					{
						
						return false;
						
					}//end try catch
					
				}//end using cmd
				
			}//end using conn
			
		}//end bool CheckPriceFieldExists
		
		
		
		
		private void AddFieldToTable(string inTable, string field, string dbType)
		{

			using (SqliteConnection sqlCon = new SqliteConnection(string.Format("Data Source={0}; Synchronous=OFF;", DataBasePath)))
			{
				
				sqlCon.Open();
				using (SqliteCommand sqlCom = sqlCon.CreateCommand())
				{
					
					sqlCom.CommandText = string.Format("ALTER TABLE {0} ADD COLUMN {1} {2}", inTable, field, dbType);
					
					sqlCom.ExecuteNonQuery();
					
				}//end using sqlCom
				
			}//end using sqlCon
			
		}//end void AddFieldToTable
	}
}