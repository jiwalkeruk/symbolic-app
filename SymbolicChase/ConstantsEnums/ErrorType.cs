// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;

namespace SymbolicChase
{
	public enum ErrorType
	{
		LocationServicesDisabled,
		ConnectionError,
		GeocodingError,
		NoLocation,
		LocationManagerFailed
	}
}

