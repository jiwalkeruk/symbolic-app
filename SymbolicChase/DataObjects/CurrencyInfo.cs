// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;

namespace SymbolicChase
{
	public class CurrencyInfo
	{

		#region Constructors

		public CurrencyInfo (string currencyCode, string currencySymbol)
		{

			this.CurrencyCode = currencyCode;
			this.CurrencySymbol = currencySymbol;

		}

		#endregion Constructors



		#region Properties

		public string CurrencyCode
		{
			get;
			private set;
		}//end string CurrencyCode



		public string CurrencySymbol
		{
			get;
			private set;
		}//end string CurrencySymbol

		#endregion Properties




		#region Overrides

		public override string ToString ()
		{
			return string.Format ("[CurrencyInfo: CurrencyCode={0}, CurrencySymbol={1}]", CurrencyCode, CurrencySymbol);
		}

		#endregion Overrides
	}
}

