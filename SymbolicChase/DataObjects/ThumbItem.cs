// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;

namespace SymbolicChase
{
	public class ThumbItem
	{

		#region Constructors

		public ThumbItem (int sortIndex, string stockReference, string thumbUrl, UIImage thumb)
		{

			this.SortIndex = sortIndex;
			this.StockReference = stockReference;
			this.ThumbUrl = thumbUrl;
			this.Thumbnail = thumb;

			this.IsWorking = true;
		}

		#endregion Cosntructors



		#region Properties

		public int SortIndex
		{
			get;
			private set;
		}//end int SortIndex




		public string StockReference
		{
			get;
			private set;
		}//end string StockReference



		public bool IsWorking
		{
			get;
			set;
		}//end bool IsWorking




		public string ThumbUrl
		{
			get;
			private set;
		}//end string ThumbUrl




		public UIImage Thumbnail
		{
			get;
			set;
		}//end UIImage Thumbnail

		#endregion Properties



		#region Overrides

		public override string ToString ()
		{
			return string.Format ("[ThumbItem: SortIndex={0}, StockReference={1}, IsWorking={2}, ThumbUrl={3}, Thumbnail={4}]", SortIndex, StockReference, IsWorking, ThumbUrl, Thumbnail);
		}

		#endregion Overrides
	}
}

