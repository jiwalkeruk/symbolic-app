// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using SQLite;

namespace SymbolicChase
{
	public class ExtraImages
	{

		#region Constructors

		public ExtraImages ()
		{
		}

		#endregion Constructors




		#region Properties

		[PrimaryKey, AutoIncrement]
		public int ID
		{
			get;
			private set;
		}//end int ID



		[Indexed]
		public string StockReference
		{
			get;
			set;
		}//end string StockReference





		public ImageSizeType ImageSizeType
		{
			get;
			set;
		}//end ImageSizeType ImageSizeType




		public string ImageUrl
		{
			get;
			set;
		}//end string ImageUrl


		#endregion Properties
	}
}

