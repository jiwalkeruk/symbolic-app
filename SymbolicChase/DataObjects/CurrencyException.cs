// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;

namespace SymbolicChase
{
	public class CurrencyException : Exception
	{
		public CurrencyException (string message, ErrorType errorType) : base(message)
		{
			this.ErrorType = errorType;

#if(DEBUG)
			Console.WriteLine("Currency exception: {0}, {1}", message, errorType);
#endif
		}



		public ErrorType ErrorType
		{
			get;
			private set;
		}//end ErrorType ErrorType
	}
}

