// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using System.Net;

namespace SymbolicChase
{
	public class RequestState<TDataObject>
	{

		public HttpWebRequest Request
		{
			get;
			set;
		}//end HttpWebRequest Request



		public TDataObject DataObject
		{
			get;
			set;
		}//end TDataObject DataObject
	}
}

