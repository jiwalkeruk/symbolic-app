// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using System.Collections.Generic;
using System.Threading;

namespace SymbolicChase
{
	public class UIExtraThumbsTableController : UITableViewController
	{
		
		#region Constructors
		
		public UIExtraThumbsTableController (IntPtr handle) : base(handle)
		{
		}
		
		
		
		public UIExtraThumbsTableController(UITableViewStyle tableViewStyle, StockItem item) : base(tableViewStyle)
		{
			
			this.Item = item;
			this.thumbCellList = new Dictionary<int, UIExtraThumbsCell>();
			
		}//end ctor
		
		#endregion Constructors
		
		
		
		#region Events
		
		public Action<UIExtraThumbsTableController, ExtraThumbSelectedEventArgs> ExtraThumbSelected;
		
		#endregion Events
		
		
		
		#region Properties
		
		public StockItem Item
		{
			get;
			private set;
		}//end StockItem Item
		
		#endregion Properties
		
		
		
		#region Fields
	
		private Dictionary<int, UIExtraThumbsCell> thumbCellList;
		
		#endregion Fields
		
		
		
		
		#region Overrides
		
		public override void LoadView ()
		{
			
			UITableView tableView = new UITableView(new RectangleF(0f, 0f, 150f, 750f), UITableViewStyle.Plain);
			this.View = tableView;
			
		}//end override void LoadView
		
		
		
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			this.LoadThumbs(null);
			
		}
		
		
		
		
		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			return true;
		}
		
		
		
		public override void WillRotate (UIInterfaceOrientation toInterfaceOrientation, double duration)
		{
			base.WillRotate (toInterfaceOrientation, duration);
			
			AppDelegate.Self.LastInterfaceOrientation = toInterfaceOrientation;
			
		}
		
		
		
//		public override void DidRotate (UIInterfaceOrientation fromInterfaceOrientation)
//		{
//			base.DidRotate(fromInterfaceOrientation);
//			
//			foreach (KeyValuePair<int, UIExtraThumbsCell> eachItem in this.thumbCellList)
//			{
//				
//				UIView.BeginAnimations("");
//				
//				eachItem.Value.ContentView.Subviews[0].Frame = new RectangleF((eachItem.Value.ContentView.Bounds.Width / 2f) - 75f, 0f, 150f, 150f);
//				
//				UIView.CommitAnimations();
//				
//			}//end foreach
//		}



		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();

			foreach (KeyValuePair<int, UIExtraThumbsCell> eachItem in this.thumbCellList)
			{
				
				UIView.BeginAnimations("");
				
				eachItem.Value.ContentView.Subviews[0].Frame = new RectangleF((eachItem.Value.ContentView.Bounds.Width / 2f) - 75f, 0f, 150f, 150f);
				
				UIView.CommitAnimations();
				
			}//end foreach
		}
		
		#endregion Overrides
		
		
		
		#region Private properties
		
		private void LoadThumbs(object data)
		{
			
			int index = 0;
			foreach (string eachItem in this.Item.LowQualityImageList)
			{
				
				UIExtraThumbsCell cell = new UIExtraThumbsCell(UITableViewCellStyle.Default, "thumbCell");
				UIWebThumbnailView webView = new UIWebThumbnailView(new RectangleF(AppDelegate.Self.DeviceType == DeviceType.iPad ? 
				                                                                   0f : 
				                                                                   (cell.ContentView.Bounds.Width / 2f) - 75f, 0f, 150f, 150f), this.Item, true);
				cell.ContentView.AddSubview(webView);
				
				this.thumbCellList.Add(index++, cell);
				
			}//end foreach
			
			this.TableView.Source = new ExtraThumbsTableSource(this);
			
			ThreadPool.QueueUserWorkItem(delegate {
				this.BeginInvokeOnMainThread(delegate {
					
					foreach (KeyValuePair<int, UIExtraThumbsCell> eachItem in this.thumbCellList)
					{
				
						(eachItem.Value.ContentView.Subviews[0] as UIWebThumbnailView).DownloadExtraImage(eachItem.Key);
				
					}//end foreach
					
				});
				
			});
			
		}//end void LoadThumbs
		
		#endregion Private properties
		
		
		
		private class ExtraThumbsTableSource : UITableViewSource
		{
			
			public ExtraThumbsTableSource(UIExtraThumbsTableController parentController)
			{
				
				this.parentController = parentController;
				
			}//end ctor
			private UIExtraThumbsTableController parentController;
			
			
			
			public override int RowsInSection (UITableView tableview, int section)
			{
				return this.parentController.thumbCellList.Count;
			}
			
			
			
			public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				
				return this.parentController.thumbCellList[indexPath.Row];
				
			}
			
			
			
			public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
			{
				
				return this.parentController.thumbCellList[indexPath.Row].ContentView.Subviews[0].Frame.Height;
				
			}
			
			
			
			public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
			{
				
				UIWebThumbnailView webView = this.parentController.thumbCellList[indexPath.Row].ContentView.Subviews[0] as UIWebThumbnailView;
				
				if (!webView.ActivityIndicator.IsAnimating)
				{
					
					//AppDelegate ad = UIApplication.SharedApplication.Delegate as AppDelegate;
					//ad.ShowSelectedExtraThumb((this.parentController.thumbCellList[indexPath.Row].ContentView.Subviews[0] as UIWebThumbnailView).Image, indexPath.Row);
					
					if (this.parentController.ExtraThumbSelected != null)
					{
						this.parentController.ExtraThumbSelected(this.parentController, 
						                                         new ExtraThumbSelectedEventArgs(indexPath.Row, 
						                                (this.parentController.thumbCellList[indexPath.Row].ContentView.Subviews[0] as UIWebThumbnailView).Image));
					}//end if
					
					if (AppDelegate.Self.DeviceType != DeviceType.iPad)
					{
						this.parentController.NavigationController.PopViewControllerAnimated(true);
					}//end if
					
				} else
				{
					tableView.DeselectRow(indexPath, true);
				}//end if else
				
			}
			
		}//end class ExtraThumbsTableSrouce
	}
	
	
	
	public class ExtraThumbSelectedEventArgs : EventArgs
	{
		
		public ExtraThumbSelectedEventArgs(int rowIndex, UIImage image)
		{
			
			this.RowIndex = rowIndex;
			this.Image = image;
			
		}//end ctor
		
		
		
		public int RowIndex
		{
			get;
			private set;
		}//end int RowIndex
		
		
		
		public UIImage Image
		{
			get;
			private set;
		}//end UIImage Image
		
	}//end class ExtraThumbSelectedEventArgs
}

