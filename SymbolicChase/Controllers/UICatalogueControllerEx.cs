// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Drawing;
using System.Collections.Generic;
using System.Threading;
using SymboliChase;
using System.Net;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;


namespace SymbolicChase
{
	public class UICatalogueControllerEx : BaseController
	{

		#region Constructors

		public UICatalogueControllerEx () : base()
		{
		}

		#endregion Constructors



		#region Fields

		private NSObject appDidBecomeActiveObserver;
		private volatile bool isRefreshing = false;
		private UICollectionView collectionView;
		private Queue<ThumbItem> thumbItemQ;

		#endregion Fields



		#region Properties

		public NSString CellID
		{
			get;
			private set;
		}//end NSString CellID



		public List<ThumbItem> Items
		{
			get;
			private set;
		}//end List<ThumbItem>



		public Queue<NSIndexPath> RefreshQ
		{
			get;
			private set;
		}//end Queue<NSIndexPath> RefreshQ


		#endregion Properties



		#region Overrides

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			this.Title = "Current Catalogue";

			this.CellID = new NSString("ThumbCell");
			this.Items = new List<ThumbItem>();
			this.thumbItemQ = new Queue<ThumbItem>();

			UICollectionViewFlowLayout flowLayout = new UICollectionViewFlowLayout();
			flowLayout.ItemSize = new SizeF(130f, 130f);
			flowLayout.ScrollDirection = UICollectionViewScrollDirection.Vertical;
			flowLayout.SectionInset = new UIEdgeInsets(10f, 10f, 10f, 10f);


			this.collectionView = new UICollectionView(this.View.Bounds, flowLayout);
			this.collectionView.RegisterClassForCell(typeof(ThumbCell), this.CellID);
			this.collectionView.BackgroundColor = UIColor.White;
			this.collectionView.AutoresizingMask = UIViewAutoresizing.All;

			this.View.AddSubview(this.collectionView);

			this.appDidBecomeActiveObserver = 
				NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.DidBecomeActiveNotification, this.AppBecomeActive);

		}



		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			if (AppDelegate.Self.RefreshRequired && !this.isRefreshing)
			{
				this.RefreshFeed();
			}//end if

		}



		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
		}




		public override bool ShouldAutorotate ()
		{
			return true;
		}




		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations ()
		{
			if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
			{
				if (AppDelegate.Self.AppStartFlag)
				{
					return UIInterfaceOrientationMask.LandscapeLeft;
				} else
				{
					return UIInterfaceOrientationMask.All;
				}//end if else
			} else
			{
				return UIInterfaceOrientationMask.AllButUpsideDown;
			}//end if else
		}



		public override void WillRotate (UIInterfaceOrientation toInterfaceOrientation, double duration)
		{
			base.WillRotate (toInterfaceOrientation, duration);

			AppDelegate.Self.LastInterfaceOrientation = toInterfaceOrientation;

		}



		#endregion Overrides



		#region Private methods

		private void RefreshFeed()
		{

			this.isRefreshing = true;

			ThreadPool.QueueUserWorkItem(delegate {

				AppDelegate.Self.Initialise();
				
				// Initialize the feed
				var feed = FeedManager.GetInstance(AppDelegate.Self.StockService);
				feed.FeedInitialisationCompleted += this.FeedManager_Initialised;
				feed.Initialise();

				if (AppDelegate.Self.AppStartFlag)
				{
					AppDelegate.Self.StartApplication();
				}//end if
				
			});	
			
		}//end void RefreshFeed




		public void FeedManager_Initialised(FeedManager sender)
		{

#if(DEBUG)
			Console.WriteLine("Feed manager initialized!");
#endif

			if (null != this.Items &&
			    this.Items.Count > 0)
			{
				this.Items.Clear();
			}//end if

			sender.FeedInitialisationCompleted -= this.FeedManager_Initialised;

			int itemIndex = 0;
			foreach (StockItem eachItem in AppDelegate.Self.StockService.GetStock())
			{

				ThumbItem thumbItem = new ThumbItem(itemIndex++, eachItem.StockReference, eachItem.LowQualityImage, null);
				this.Items.Add(thumbItem);
				this.thumbItemQ.Enqueue(thumbItem);

			}//end foreach

			this.InvokeOnMainThread(delegate {

				this.collectionView.Source = new CatalogueSource(this);
				this.collectionView.ReloadData();

			});
			
			AppDelegate.Self.LastRefreshedDate = DateTime.Now;
			this.isRefreshing = false;

#if(DEBUG)
			Console.WriteLine("Last refresh date in FeedManager_Initialized: {0}", AppDelegate.Self.LastRefreshedDate);
			Console.WriteLine("Is refreshing: {0}", this.isRefreshing);
#endif

			
//			this.StartThumbLoad();

			this.DownloadThumbs();

		}










		private async void DownloadThumbs()
		{
			
//			ThreadPool.QueueUserWorkItem(delegate {
//
//				while (this.thumbItemQ.Count > 0)
//				{
//
//					ThumbItem thumbItem = this.thumbItemQ.Dequeue();
//
//					byte[] thumbBuffer = AppDelegate.Self.StockService.GetCachedImage(thumbItem.StockReference);
//
//					bool updateThumb = false;
//
//					if (null != thumbBuffer)
//					{
//						using (NSData imgData = NSData.FromArray(thumbBuffer))
//							using (UIImage thumbImage = UIImage.LoadFromData(imgData))
//						{
//							if (thumbImage.Size.Width == 0 ||
//							    thumbImage.Size.Height == 0)
//							{
//								updateThumb = true;
//							} else
//							{
//								thumbItem.Thumbnail = RenderingHelper.CreateScaledImageInRect(thumbImage, new RectangleF(0f, 0f, 130f, 130f));
//								thumbItem.IsWorking = false;
//							}//end if else
//
//						}//end usings
//
//					} else
//					{
//						updateThumb = true;
//					}//end if else
//
//					if (updateThumb)
//					{
//
//						ThreadPool.QueueUserWorkItem(delegate {
//
//							var request = new NSUrlRequest(new NSUrl(thumbItem.ThumbUrl));
//
//							this.InvokeOnMainThread(delegate {
//								new NSUrlConnection(request, new ConnectionDelegate(AppDelegate.Self.StockService, thumbItem, this.ThumbDownloaded), true);
//							});
//
//						});
//
//					}//end if
//
//				}//end while
//
//			});


			Dictionary<int, Task<Tuple<ThumbItem, byte[]>>> thumbTasks = new Dictionary<int, Task<Tuple<ThumbItem, byte[]>>>();
			RectangleF scaledImageRect = new RectangleF(0f, 0f, 130f, 130f);

			while (thumbItemQ.Count > 0)
			{

				ThumbItem thumbItem = this.thumbItemQ.Dequeue();
				List<NSIndexPath> paths = new List<NSIndexPath>();

				byte[] localThumbBuffer = AppDelegate.Self.StockService.GetCachedImage(thumbItem.StockReference);
				bool updateThumb = false;

				if (null != localThumbBuffer)
				{

					using (NSData imgData = NSData.FromArray(localThumbBuffer))
						using (UIImage thumbImage = UIImage.LoadFromData(imgData))
					{

						if (thumbImage.Size.Width == 0 ||
						    thumbImage.Size.Height == 0)
						{
							updateThumb = true;
						} else
						{

							thumbItem.Thumbnail = RenderingHelper.CreateScaledImageInRect(thumbImage, scaledImageRect);
							thumbItem.IsWorking = false;

							paths.Add(NSIndexPath.FromItemSection(thumbItem.SortIndex, 0));

						}//end if else

					}//end usings

				} else
				{
					updateThumb = true;
				}//end if else

				if (updateThumb)
				{

					thumbTasks.Add(thumbItem.SortIndex, this.GetThumbForItemAsync(thumbItem));

					if (thumbTasks.Count == 15 ||
					    thumbItemQ.Count < 15)
					{

						while (thumbTasks.Count > 0)
						{

							Task<Tuple<ThumbItem, byte[]>> firstFinishedTask = await Task.WhenAny(thumbTasks.Values.ToArray());
							Tuple<ThumbItem, byte[]> result = await firstFinishedTask;

							thumbTasks.Remove(result.Item1.SortIndex);

							using (NSData imgData = NSData.FromArray(result.Item2))
							{
								result.Item1.Thumbnail = RenderingHelper.CreateScaledImageInRect(UIImage.LoadFromData(imgData), 
								                                                                 new RectangleF(0f, 0f, 130f, 130f));
							}//end using imgData
							result.Item1.IsWorking = false;
							AppDelegate.Self.StockService.UpdateStockThumb(result.Item1.StockReference, result.Item2);

							paths.Add(NSIndexPath.FromItemSection(result.Item1.SortIndex, 0));
					
						}//end while

					}//end if

				}//end if

				if (paths.Count > 0)
				{

					this.BeginInvokeOnMainThread(delegate {

						this.collectionView.ReloadItems(paths.ToArray());

					});

				}//end if

			}//end while


		}//end void DownloadThumbs



		private async Task<Tuple<ThumbItem, byte[]>> GetThumbForItemAsync(ThumbItem thumbItem)
		{

			using (HttpClient httpClient = new HttpClient())
			{

				return new Tuple<ThumbItem, byte[]>(thumbItem, await httpClient.GetByteArrayAsync(thumbItem.ThumbUrl));

			}//end using httpClient
				

		}//end Task<byte[]> GetThumbForItem




		private List<NSIndexPath> updateList = new List<NSIndexPath>();
		private void ThumbDownloaded(ThumbItem thumbItem)
		{
			this.updateList.Add(NSIndexPath.FromItemSection(thumbItem.SortIndex, 0));
		
			if (this.updateList.Count == 10 ||
			    this.Items.Count(s => s.IsWorking) < 10)
			{
				NSIndexPath[] updatePaths = this.updateList.ToArray();
				this.collectionView.ReloadItems(updatePaths);
				this.updateList.Clear();
			}//end if

		}//end void ThumbDownloaded
		
		






		private void AppBecomeActive(NSNotification ntf)
		{

			if (AppDelegate.Self.RefreshRequired && !AppDelegate.Self.AppStartFlag)
			{
				this.RefreshFeed();
			}//end if
			
		}//end void AppBeomeActive

		#endregion Private methods




		#region Public methods

		public void ItemSelected(ThumbItem thumbItem)
		{

			if (!thumbItem.IsWorking)
			{
				StockItem stockItem = AppDelegate.Self.StockService.GetStockItem(thumbItem.StockReference);

				if (null != stockItem)
				{
					AppDelegate.Self.ShowStockDetail(stockItem, UIImage.FromImage(thumbItem.Thumbnail.CGImage));
				}//end if
			}//end if

		}//end void ItemSelected

		#endregion Public methods
	}
}

