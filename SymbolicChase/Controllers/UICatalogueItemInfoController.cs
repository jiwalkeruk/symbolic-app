using MonoTouch.UIKit;
using System.Drawing;
using System;
using MonoTouch.Foundation;

namespace SymbolicChase
{
	public partial class UICatalogueItemInfoController : BaseController
	{
		
		#region Constructors
		
		public UICatalogueItemInfoController () : base ("UICatalogueItemInfoController", null)
		{
		}
		
		#endregion Constructors
		
		
		
		
		#region Properties
		
		public StockItem StockItem
		{
			get;
			set;
		}//end StockItem StockItem
		
		#endregion Properties
		
		
		
		
		#region Overrides
				
		public override async void ViewDidLoad ()
		{

			base.ViewDidLoad ();

			View.BackgroundColor = UIColor.Clear;
			View.AddSubview(RenderingHelper.CreateStockDescriptionView(StockItem));
		}
		
		
		
		
		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			return true;
		}
		
		#endregion Overrides
	}
}

