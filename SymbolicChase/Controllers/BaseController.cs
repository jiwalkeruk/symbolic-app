// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Drawing;
using SymboliChase;

namespace SymbolicChase
{
	public class BaseController : UIViewController
	{
		
		#region Constructors
		
		public BaseController (string nibName, NSBundle mainBundle) : base(nibName, mainBundle)
		{
		}
		
		
		
		public BaseController() : base()
		{
		}
		
		#endregion Constructors
		
		
		
		#region Overrides
		
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			
			this.View.Frame = UIScreen.MainScreen.Bounds;
			
		}
		
		#endregion Overrides
	}
}

