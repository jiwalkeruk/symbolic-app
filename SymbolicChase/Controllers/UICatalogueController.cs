using System;
using MonoTouch.UIKit;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using System.Diagnostics;
using System.Threading;
using System.Linq;
using SymboliChase;
using System.Net;
using System.IO;

namespace SymbolicChase
{
	public partial class UICatalogueController : BaseController
	{
		#region Constructors

		// The IntPtr and initWithCoder constructors are required for controllers that need 
		// to be able to be created from a xib rather than from managed code

		public UICatalogueController () : base()
		{
			Initialize ();
		}

		void Initialize ()
		{
			this.threadLock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
			Title = "Current Catalogue";
		}
		
		#endregion
		
		
		
		#region Fields
		
		private UIScrollView scrollView;		
		private bool initialOrientation = true;
		private NSObject appDidBecomeActiveObserver;
		private volatile bool isRefreshing = false;
		private Queue<UIWebThumbnailView> pThumbQ;
		private ReaderWriterLockSlim threadLock;
		
		#endregion Fields



		#region Properties

		public Queue<UIWebThumbnailView> ThumbQ
		{
			get
			{
				try
				{
					this.threadLock.EnterReadLock();

					return this.pThumbQ;

				} finally
				{

					if (this.threadLock.IsReadLockHeld)
					{
						this.threadLock.ExitReadLock();
					}//end if

				}//end try finally
			}//end get

		}//end Queue<UIWebThumbnailView> ThumbQ

		#endregion Properties
		
		
		
		public override void ViewDidLoad ()
		{
			
			base.ViewDidLoad ();
			this.pThumbQ = new Queue<UIWebThumbnailView>();

			this.View.BackgroundColor = UIColor.White;

			this.scrollView = new UIScrollView(this.View.Bounds);
			
			this.scrollView.BackgroundColor = UIColor.White;
			this.scrollView.DraggingEnded += ScrollView_DraggingEnded;
			this.View.AddSubview(this.scrollView);
			
			this.appDidBecomeActiveObserver = 
				NSNotificationCenter.DefaultCenter.AddObserver(UIApplication.DidBecomeActiveNotification, this.AppBecomeActive);
			
			this.RefreshFeed();
			
		}




		void ScrollView_DraggingEnded (object sender, DraggingEventArgs e)
		{

//			this.DownloadVisibleItems();

		}



		private void DownloadVisibleItems()
		{

			foreach (UIView eachFrameView in this.scrollView.Subviews)
			{
				if (eachFrameView is UIFrameView)
				{
					
					UIFrameView frameView = (UIFrameView)eachFrameView;
					if (frameView.Frame.Location.Y - this.scrollView.ContentOffset.Y < this.scrollView.Frame.Height)
					{
						if (!frameView.GetIsDownloaded())
						{
							if (!frameView.GetIsDownloading())
							{
								frameView.StartThumbDownload();
							}//end if
							
						}//end if
					}//end if
					
				}//end if
				
			}//end foreach

		}//end void DownloadVisibleItems
		
		
		
		public override void ViewDidUnload ()
		{
			base.ViewDidUnload ();
			
			NSNotificationCenter.DefaultCenter.RemoveObserver(this.appDidBecomeActiveObserver);
		}
		
		
		
		
		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			
			this.initialOrientation = false;
			
		}
		
		
		
		
		public void FeedManager_Initialised(FeedManager sender)
		{
				
			sender.FeedInitialisationCompleted -= this.FeedManager_Initialised;
			
			var numberOfFrames = ScrollViewHelper.FrameCount;
			
			//set up the frame
			var scrollFrame = ScrollViewHelper.CreateScrollFrame();
			
			var originalHeight = scrollFrame.Height ;
			scrollFrame.Height = originalHeight * numberOfFrames;
			
	    	
			var newFrame = ScrollViewHelper.CreateScrollFrame();

			this.InvokeOnMainThread(delegate {

				scrollView.Frame = newFrame;
				scrollView.ContentSize = scrollFrame.Size;

			});
		    for (int frameIndex = 0; frameIndex < numberOfFrames; frameIndex ++) 
			{
								
				var location = new PointF();
				location.Y = originalHeight * frameIndex;
			
				newFrame.Location = location;
			
				UIFrameView frameView = null;
				this.InvokeOnMainThread(delegate {

					frameView = new UIFrameView(frameIndex, newFrame);
					scrollView.AddSubview(frameView);

					foreach (UIWebThumbnailView eachThumbView in frameView.ThumbViews)
					{
						this.ThumbQ.Enqueue(eachThumbView);
					}//end foreach

				});

				
			}
		
			AppDelegate.Self.LastRefreshedDate = DateTime.Now;
			this.isRefreshing = false;

//			this.BeginInvokeOnMainThread(delegate {
//
//				this.DownloadVisibleItems();
//
//			});

			this.StartThumbLoad();

		}



		private void StartThumbLoad()
		{

			ThreadPool.QueueUserWorkItem(delegate {

				while (this.ThumbQ.Count > 0)
				{

					UIWebThumbnailView thumbView = this.ThumbQ.Dequeue();

					byte[] thumbBuffer = AppDelegate.Self.StockService.GetCachedImage(thumbView.StockItem.StockReference);

					bool updateThumb = false;

					if (null != thumbBuffer)
					{

						using (NSData imgData = NSData.FromArray(thumbBuffer))
							using (UIImage thumbImage = UIImage.LoadFromData(imgData))
						{

							if (thumbImage.Size.Width == 0 ||
							    thumbImage.Size.Height == 0)
							{
								updateThumb = true;
							} else
							{
								thumbView.RenderImageWithAspectRatio(thumbImage);
							}//end if else

						}//end usings

					} else
					{
						updateThumb = true;
					}//end if else

					if (updateThumb)
					{

						this.InvokeOnMainThread(delegate {

							if (null != thumbView.Image)
							{
								thumbView.Image.Dispose();
								thumbView.Image = null;
							}//end if

							thumbView.ActivityIndicator.StartAnimating();

						});

						this.DownloadThumb(thumbView);

					}//end if

				}//end while

			});

		}//end void StartThumbLoad



		private void DownloadThumb(UIWebThumbnailView thumbView)
		{

			HttpWebRequest request = WebRequest.Create(thumbView.StockItem.LowQualityImage) as HttpWebRequest;

			Console.WriteLine("Link: {0}", thumbView.StockItem.LowQualityImage);

			request.Method = "GET";
			request.KeepAlive = true;

			request.BeginGetResponse(this.DownloadThumbCallback, new RequestState<UIWebThumbnailView> { Request = request, DataObject = thumbView });


		}//end void DownloadThumb




		private void DownloadThumbCallback(IAsyncResult asRes)
		{

			HttpWebResponse response = null;
			RequestState<UIWebThumbnailView> state = null;

			try
			{

				state = asRes.AsyncState as RequestState<UIWebThumbnailView>;
				response = state.Request.EndGetResponse(asRes) as HttpWebResponse;

				byte[] imgBuffer = null;
				using (MemoryStream ms = new MemoryStream())
				{
					byte[] buffer = new byte[524288];
					using (Stream responseStream = response.GetResponseStream())
					{

						while (true)
						{
							int readCount = responseStream.Read(buffer, 0, buffer.Length);
							if (readCount == 0)
							{
								break;
							}//end if

							ms.Write(buffer, 0, readCount);

						}//end while

					}//end using responseStream

					imgBuffer = ms.ToArray();

					Console.WriteLine("Downloaded image size: {0}", imgBuffer.Length);

				}//end using ms

				this.BeginInvokeOnMainThread(delegate {

					using (NSData imgData = NSData.FromArray(imgBuffer))
						using (UIImage thumbImage = UIImage.LoadFromData(imgData))
					{
						state.DataObject.RenderImageWithAspectRatio(thumbImage);
					}//end usings

				});

				AppDelegate.Self.StockService.UpdateStockThumb(state.DataObject.StockItem.StockReference, imgBuffer);

			} catch (Exception ex)
			{
				Console.WriteLine("Error downloading image for stock ref: {0}. Exception: {1}--{2}", state.DataObject.StockItem.StockReference, ex.Message, ex.StackTrace);
			} finally
			{

				if (null != response)
				{
					response.Close();
				}//end if

			}//end try catch finally


		}//end void DownloadThumbCallback
		
		
		
		
		
		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			if (AppDelegate.Self.AppStartFlag && AppDelegate.Self.DeviceType == DeviceType.iPad)
			{
				return toInterfaceOrientation == UIInterfaceOrientation.LandscapeLeft;
			} else
			{
				return true;
			}//end if else
		}
		
		
		
			
		public override void WillRotate (UIInterfaceOrientation toInterfaceOrientation, double duration)
		{
			base.WillRotate (toInterfaceOrientation, duration);
			
			AppDelegate.Self.LastInterfaceOrientation = toInterfaceOrientation;
			if (!this.initialOrientation)
			{
				
				UIView.BeginAnimations("");
				UIView.SetAnimationDuration(duration);
				UIView.SetAnimationCurve(UIViewAnimationCurve.EaseOut);
				
				this.AdjustOnOrientation(toInterfaceOrientation);
				
				UIView.CommitAnimations();
				
			}//end if
		}



		public override void DidRotate (UIInterfaceOrientation fromInterfaceOrientation)
		{
			base.DidRotate (fromInterfaceOrientation);

//			this.DownloadVisibleItems();
		}
		
		
		
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			
			if (!this.initialOrientation)
			{
				
				this.AdjustOnOrientation(this.InterfaceOrientation);
				
			}//end if
			
			
			if (AppDelegate.Self.RefreshRequired && !this.isRefreshing)
			{
				this.RefreshFeed();
			}//end if
						
		}
		
		
		
		private void RefreshFeed()
		{

			this.isRefreshing = true;
			
			if (this.scrollView.Subviews.Length > 0)
			{
				
				foreach (UIFrameView eachFrameView in this.scrollView.Subviews.Where(s => s is UIFrameView))
				{
					eachFrameView.RemoveFromSuperview();
				}//end foreach
				
			}//end if
			
			ThreadPool.QueueUserWorkItem(delegate {
										
				AppDelegate.Self.Initialise();

				// Initialize the feed
				var feed = FeedManager.GetInstance(AppDelegate.Self.StockService);
				feed.FeedInitialisationCompleted += this.FeedManager_Initialised;
				feed.Initialise();

				if (AppDelegate.Self.AppStartFlag)
				{
					AppDelegate.Self.StartApplication();
				}//end if
				
			});	
			
		}//end void RefreshFeed
		
		
		
		private void AppBecomeActive(NSNotification ntf)
		{
			
			if (AppDelegate.Self.RefreshRequired && !AppDelegate.Self.AppStartFlag)
			{
				this.RefreshFeed();
			}//end if
			
		}//end void AppBeomeActive
		
		
				
		private void AdjustOnOrientation(UIInterfaceOrientation forOrientation)
		{
			
			var numberOfFrames = ScrollViewHelper.FrameCount;
			var scrollFrame = ScrollViewHelper.CreateScrollFrame();
			
			var originalHeight = scrollFrame.Height;
			scrollFrame.Height = originalHeight * numberOfFrames;
			
			var newFrame = ScrollViewHelper.CreateScrollFrame();
			
			scrollView.Frame = newFrame;
			
			scrollView.ContentSize = scrollFrame.Size;
			
			
			List<UIFrameView> frameViewList = this.scrollView.Subviews.OfType<UIFrameView>().ToList();
			for (int frameIndex = 0; frameIndex < numberOfFrames; frameIndex ++)
			{
				
				var location =  new PointF();
				location.Y = originalHeight * frameIndex;
				
				newFrame.Location = location;
				
				frameViewList[frameIndex].Frame = newFrame;
				frameViewList[frameIndex].AdjustThumbsForOrientation(forOrientation);
				
			}//end for
			
		}//end void AdjustOnOrientation
	}
}
