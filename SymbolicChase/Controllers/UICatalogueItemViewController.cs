using MonoTouch.UIKit;
using System.Drawing;
using System;
using MonoTouch.Foundation;
using SymboliChase;
using MonoTouch.MessageUI;

namespace SymbolicChase
{
	public partial class UICatalogueItemViewController : BaseController
	{
		
		#region Constructors
		
		public UICatalogueItemViewController () : base ("UICatalogueItemViewController", null)
		{
			this.Initialize();
		}
		
		
		
		public UICatalogueItemViewController (StockItem stock, IStockService service, UIImage thumbImage) : base("UICatalogueItemViewController", null)
		{
			StockItem = stock;
			
			_service = service;
			this.thumbImage = thumbImage;
			
			this.Initialize();
		}

		void Initialize ()
		{
		}
		
		#endregion Constructors
		
		
		
		#region Fields
		
		private const int DescriptiveTagId = 678;
		private IStockService _service;
		private UIWebImageView _image;
		private UIImage thumbImage;
		private bool addDescView = false;
		private UIButton moreViewsButton, shareButton;
		private UIView buttonContainer;
		private UIBarButtonItem buttonItem;
		private UIPopoverController popoverController;
		private UIExtraThumbsTableController extraThumbsController;
		private UICatalogueItemInfoController _descriptionController;
		private bool showsMoreViewsPopover;
		private UIActionSheet shareActionSheet;
		private MFMailComposeViewController mailController;
		
		#endregion Fields
		
		
		
		#region Properties
		
		public StockItem StockItem
		{
			get;
			private set;
		}//end StockItem StockItem
		
		#endregion Properties
		
		
		
		#region Overrides
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			this.moreViewsButton = UIButton.FromType(UIButtonType.Custom);
			this.moreViewsButton.ContentMode = UIViewContentMode.ScaleAspectFit;
			this.moreViewsButton.SetBackgroundImage(UIImage.FromBundle("Images/icon-moreviews"), UIControlState.Normal);
			this.moreViewsButton.Frame = new RectangleF(0f, 0f, 30f, 30f);
			this.moreViewsButton.TouchUpInside += this.MoreViewsButton_Tapped;
			this.moreViewsButton.AutoresizingMask = UIViewAutoresizing.All;
			
			this.shareButton = UIButton.FromType(UIButtonType.Custom);
			this.shareButton.ContentMode = UIViewContentMode.ScaleAspectFit;
			this.shareButton.SetBackgroundImage(UIImage.FromBundle("Images/icon-share"), UIControlState.Normal);
			this.shareButton.Frame = new RectangleF(this.moreViewsButton.Frame.Right + 20f, 0f, 30f, 30f);
			this.shareButton.TouchUpInside += this.ShareButton_Tapped;
			this.shareButton.AutoresizingMask = UIViewAutoresizing.All;
			
			this.buttonContainer = new UIView(new RectangleF(0f, 0f, 80f, 30f));
			this.buttonContainer.AutosizesSubviews = true;
			this.buttonContainer.AddSubview(this.moreViewsButton);
			this.buttonContainer.AddSubview(this.shareButton);
			
			this.buttonItem = new UIBarButtonItem(this.buttonContainer);
			
			View.Frame = new RectangleF(0f, 20f, ScrollViewHelper.ScreenWidth, ScrollViewHelper.ScreenHeight);
			View.AutosizesSubviews = true;
			
			this.DisplayImage(-1);
				
			this.StockItem.LowQualityImageList = this._service.GetExtraImagesForStock(this.StockItem.StockReference, ImageSizeType.Low);
			
		}

				
		
		
		
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			
			this.NavigationItem.SetRightBarButtonItem(this.buttonItem, true);
		}
		
		
		
		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			
//			if (AppDelegate.Self.IsFirstRun)
			if (!AppDelegate.Self.ToolTipShown)
			{
				
//				AppDelegate.Self.IsFirstRun = true;
				AppDelegate.Self.ToolTipShown = true;
				
				using (UIAlertView alertView = new UIAlertView())
				{
					
					alertView.Title = "Usage";
					alertView.Message = "Tap to show item description, pinch to go back.";
					alertView.AddButton("OK");
					
					alertView.Show();
					
				}//end using
				
			}//end if
		}
		
		
		
		
		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			
			if (this.popoverController != null)
			{
				this.popoverController.Dismiss(true);
			}//end if
		}
		
		
		
		
		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			return true;
		}
		
		
		
		public override void WillRotate (UIInterfaceOrientation toInterfaceOrientation, double duration)
		{
			base.WillRotate (toInterfaceOrientation, duration);
			
			AppDelegate.Self.LastInterfaceOrientation = toInterfaceOrientation;
			if (null != this._descriptionController)
			{
				
				UIItemDescriptionView itemDescriptionView = this._descriptionController.View.Subviews[0] as UIItemDescriptionView;
				itemDescriptionView.PinchReceived -= this.ItemDescriptionView_PinchReceived;
				this._descriptionController.View.RemoveFromSuperview();
				this.addDescView = true;
				
			} else
			{
				this.addDescView = false;
			}//end if else
			
			if (this.showsMoreViewsPopover)
			{
				this.popoverController.Dismiss(true);
			}//end if
			
			
			if (AppDelegate.Self.DeviceType == DeviceType.iPhone)
			{
				
				RectangleF buttonContainerFrame = this.buttonContainer.Frame;
				RectangleF moreViewsButtonFrame = this.moreViewsButton.Frame;
				RectangleF shareButtonFrame = this.shareButton.Frame;
				
				if (toInterfaceOrientation == UIInterfaceOrientation.LandscapeLeft || 
				    toInterfaceOrientation == UIInterfaceOrientation.LandscapeRight)
				{
				
					buttonContainerFrame.Height = 24f;
					moreViewsButtonFrame.Height = 24f;
					moreViewsButtonFrame.Width = 24f;
					shareButtonFrame.Height = 24f;
					shareButtonFrame.Width = 24f;
					this.buttonContainer.Frame = buttonContainerFrame;
					this.moreViewsButton.Frame = moreViewsButtonFrame;
					this.shareButton.Frame = shareButtonFrame;
				
				} else
				{
				
					buttonContainerFrame.Height = 30f;
					moreViewsButtonFrame.Height = 30f;
					moreViewsButtonFrame.Width = 30f;
					shareButtonFrame.Height = 30f;
					shareButtonFrame.Width = 30f;
					this.buttonContainer.Frame = buttonContainerFrame;
					this.moreViewsButton.Frame = moreViewsButtonFrame;
					this.shareButton.Frame = shareButtonFrame;
				
				}//end if else
				
			}//end if
			
		}
		
		
		
		public override void DidRotate (UIInterfaceOrientation fromInterfaceOrientation)
		{
			base.DidRotate (fromInterfaceOrientation);
			
			if (this.addDescView)
			{
				
				this._descriptionController = new UICatalogueItemInfoController();
				this._descriptionController.StockItem = StockItem;
				this._descriptionController.View.Tag = DescriptiveTagId;
				
				UIItemDescriptionView itemDescriptionView = _descriptionController.View.Subviews[0] as UIItemDescriptionView;
				itemDescriptionView.PinchReceived += this.ItemDescriptionView_PinchReceived;
				
				UIView.BeginAnimations("Fade");
				UIView.SetAnimationDuration(1.00);
				UIView.SetAnimationTransition(UIViewAnimationTransition.FlipFromLeft, _descriptionController.View, true);
				
				this.View.AddSubview(this._descriptionController.View);
				
				UIView.CommitAnimations();
				
			}//end if
			
			if (this.showsMoreViewsPopover)
			{
				RectangleF moreButtonRectInMainView = this.moreViewsButton.ConvertRectToView(this.moreViewsButton.Frame, this.View);
				this.popoverController.PresentFromRect(moreButtonRectInMainView, this.View, UIPopoverArrowDirection.Any, true);
			}//end if
		}
		
		
		
		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			
			base.TouchesEnded (touches, evt);
			
			if (_descriptionController == null) 
			{
				_descriptionController = new UICatalogueItemInfoController();
			
				_descriptionController.StockItem = StockItem;
				 
				UIView.BeginAnimations("Fade");
				UIView.SetAnimationDuration(1.00);
				UIView.SetAnimationTransition(UIViewAnimationTransition.FlipFromLeft, _descriptionController.View, true);
				
				BlurImage();
				
				_descriptionController.View.Tag = DescriptiveTagId;
				
				UIItemDescriptionView itemDescriptionView = _descriptionController.View.Subviews[0] as UIItemDescriptionView;
				itemDescriptionView.PinchReceived += this.ItemDescriptionView_PinchReceived;
					
				View.Add(_descriptionController.View);
				
				UIView.CommitAnimations();
			} 
		}
		
		#endregion Overrides
		
		
		
		#region Public methods
		
		public void ChangeLargeImage(UIImage image, int index)
		{
			
			this.thumbImage.Dispose();
			this.thumbImage = null;
			this.thumbImage = image;
			_image.RemoveFromSuperview();
			this.DisplayImage(index);
			
			if (this.popoverController != null)
			{
				this.popoverController.Dismiss(true);
			}//end if
			
		}//end void ChangeLargeImage
		
		
		
		
		public void BlurImage() 
		{
			_image.Alpha = 0.2f;
		}
		
		
		
		public void FocusImage() 
		{
			_image.Alpha = 1.0f;	
		}
		
		#endregion Public methods
		
		
		
		
		#region Private methods
		
		private void ItemDescriptionView_PinchReceived(UIItemDescriptionView itemDescView)
		{
			
			itemDescView.PinchReceived -= this.ItemDescriptionView_PinchReceived;
			
			UIView.BeginAnimations("Fade");
			UIView.SetAnimationDuration(1.00);
			
			var descView = View.ViewWithTag(DescriptiveTagId);
			descView.Alpha = 0.0f;
			FocusImage();

			UIView.CommitAnimations();
			
			_descriptionController.View.RemoveFromSuperview();
			_descriptionController = null;
			
		}
		
		
		
		private void DisplayImage(int index)
		{
			
			//setup the image
			//_image = new UIWebFullImageView(View.Frame, StockItem, _service.GetCachedImage(StockItem.StockReference));
			_image = new UIWebFullImageView(View.Bounds, StockItem, this.thumbImage);
			_image.AutoresizingMask = UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleLeftMargin |
				UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleWidth;
			//_image.Center = View.Center;
			
			if (index == -1)
			{
				_image.Download();
			} else
			{
				_image.DownloadExtraImage(index);
			}//end if else
			
			//View.AddSubview(_image);
			View.InsertSubview(_image, 0);
			
		}//end void DisplayImage
		
		
		
		
		private void MoreViewsButton_Tapped(object sender, EventArgs e)
		{
			
			if (AppDelegate.Self.DeviceType == DeviceType.iPad)
			{
				this.PresentMoreViewsPopover();
			} else
			{
				
				this.PushMoreViewsController();
				
			}//end if else
			
		}//end void MoreViewsButton_Tapped
		
		
		
		
		
		private void ShareButton_Tapped (object sender, EventArgs e)
		{
			
			this.shareActionSheet = new UIActionSheet();
			this.shareActionSheet.AddButton("Share");
			this.shareActionSheet.AddButton("Enquire");
			this.shareActionSheet.AddButton("Cancel");
			this.shareActionSheet.CancelButtonIndex = 2;
			this.shareActionSheet.Clicked += delegate(object actionSender, UIButtonEventArgs args) {
				
				switch (args.ButtonIndex)
				{
				case 0: // share
					
					this.ShareItem();
					
					break;
					
				case 1:
					
					this.EnquireItem();
					
					break;
					
				default:
					
					//do nothing
					break;
					
				}//end switch
				
			};
			
			this.shareActionSheet.ShowInView(this.View);
			
		}
		
		
		
		
		private void ShareItem()
		{
			
			if (MFMailComposeViewController.CanSendMail)
			{
				
				string stockTitle = this.StockItem.StockTitle;
				string guideText = "Click the link below to view full item details:";
				string link = string.Format("http://www.symbolicchase.com/Public/Stock/View.aspx?ref={0}", this.StockItem.StockReference);
				
				this.mailController = new MFMailComposeViewController();
				this.mailController.SetSubject("I found an item on Symbolic & Chase that might interest you");
				this.mailController.SetMessageBody(string.Format("{0}\n\n{1}\n{2}", stockTitle, guideText, link), false);
				this.mailController.AddAttachmentData(this.thumbImage.AsJPEG(), "image/jpeg", StockItem.StockReference + ".jpg");
				this.mailController.Finished += this.MailController_Finished;

				this.PresentViewController(this.mailController, true, null);
				
			} else
			{
				
				using (UIAlertView alertView = new UIAlertView())
				{
					alertView.Title = "Cannot Send Email";
					alertView.Message = "Device cannot send email.";
					alertView.AddButton("OK");
					alertView.Show();
				}//end using alertView
				
			}//end if else				
			
		}//end void ShareItem
		
		
		
		
		
		private void EnquireItem()
		{
			
			if (MFMailComposeViewController.CanSendMail)
			{
				
				this.mailController = new MFMailComposeViewController();
				this.mailController.SetToRecipients(new string[] { "info@symbolicchase.com" });
				this.mailController.SetSubject("Symbolic & Chase app enquiry");
				this.mailController.SetMessageBody(this.StockItem.StockTitle, false);
				this.mailController.AddAttachmentData(this.thumbImage.AsJPEG(), "image/jpeg", StockItem.StockReference + ".jpg");
				this.mailController.Finished += MailController_Finished;

				this.PresentViewController(this.mailController, true, null);
				
				
			} else
			{
				
				using (UIAlertView alertView = new UIAlertView())
				{
					alertView.Title = "Cannot Send Email";
					alertView.Message = "Device cannot send email.";
					alertView.AddButton("OK");
					alertView.Show();
				}//end using alertView
				
			}//end if else
			
		}//end void EnquireItem
		
		
		
			
			
			
		private void MailController_Finished (object sender, MFComposeResultEventArgs e)
		{
			
			if (e.Result == MFMailComposeResult.Failed)
			{
				
				using (UIAlertView alertView = new UIAlertView())
				{
					alertView.Title = "Problem Sending Email";
					alertView.Message = "Please try again.";
					alertView.AddButton("OK");
					alertView.Show();
				}
				
			}//end if

			this.mailController.DismissViewController(true, null);
			
		}
		
		
		
		
		private void ExtraThumbsController_Selected(UIExtraThumbsTableController sender, ExtraThumbSelectedEventArgs args)
		{
			
			this.ChangeLargeImage(args.Image, args.RowIndex);
			
			if (this._descriptionController != null)
			{
				
				UIView.BeginAnimations("Fade");
				UIView.SetAnimationDuration(1.00);
				
				BlurImage();
				
				UIView.CommitAnimations();
				
			}//end if
			
		}//end void ExtraThumbController_Selected
		
		
		
		// For iPad
		private void PresentMoreViewsPopover()
		{
			
			if (null == this.popoverController)
			{
				this.popoverController = new UIPopoverController((this.extraThumbsController = new UIExtraThumbsTableController(UITableViewStyle.Plain, this.StockItem)));
				this.popoverController.DidDismiss += (sender, e) => {
					
					this.showsMoreViewsPopover = false;
					
				};
				this.StockItem.HighQualityImageList = _service.GetExtraImagesForStock(this.StockItem.StockReference, ImageSizeType.High);
				this.extraThumbsController.ExtraThumbSelected += this.ExtraThumbsController_Selected;
			}//end if
			
			float popoverContentHeight = 150f * this.StockItem.HighQualityImageList.Count;
			if (popoverContentHeight > 750f)
			{
				popoverContentHeight = 750f;
			}//end if
			this.popoverController.SetPopoverContentSize(new SizeF(150f, popoverContentHeight), false);
			
			this.showsMoreViewsPopover = true;
			RectangleF moreButtonRectInMainView = this.moreViewsButton.ConvertRectToView(this.moreViewsButton.Frame, this.View);
			this.popoverController.PresentFromRect(moreButtonRectInMainView, this.View, UIPopoverArrowDirection.Any, true);
			
		}//end void PresentMoreViewsPopover
		
		
		
		// For iPhone
		private void PushMoreViewsController()
		{
			
			this.extraThumbsController = new UIExtraThumbsTableController(UITableViewStyle.Plain, this.StockItem);
			this.StockItem.HighQualityImageList = _service.GetExtraImagesForStock(this.StockItem.StockReference, ImageSizeType.High);
			this.extraThumbsController.ExtraThumbSelected += this.ExtraThumbsController_Selected;
			
			this.NavigationController.PushViewController(this.extraThumbsController, true);
			
		}//end void PushMoreViewsController
		
		#endregion Private methods
		
		
	}
}

