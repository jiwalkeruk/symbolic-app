// Created by Dimitris Tavlikos, dimitris@tavlikos.com, http://software.tavlikos.com
using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;

namespace SymbolicChase
{
	public class MainNavController : UINavigationController
	{

		#region Constructors

		public MainNavController (UIViewController rootController) : base(rootController)
		{
		}

		#endregion Constructors




		#region Overrides


		public override bool ShouldAutorotate ()
		{
			return true;
		}



		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations ()
		{

			if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
			{
				if (AppDelegate.Self.AppStartFlag)
				{
					return UIInterfaceOrientationMask.LandscapeLeft;
				} else
				{
					return UIInterfaceOrientationMask.All;
				}//end if else

			} else
			{

				return UIInterfaceOrientationMask.AllButUpsideDown;

			}//end if else
		}

		#endregion Overrides
	}
}

